[Back to readme](../README.md)

> **SCHEMA**

- [TripHdAddDtoInput](../schema/inputs/trip.md#triphdadddtoinput)
- [TripDto](../schema/objects/trip.md#tripdto)

# TripCreate

> **MUTATION**

```
mutation tripCreate($input: TripHdAddDtoInput) {
  tripCreate(input: $input) {
    ...TripDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "vehicleId": "",
    "licensePlate": "",
    "startTrip": "",
    "agentId": "",
    "driverId": "",
    "helperId": "",
    "driverLicens": "",
    "tripDetail": {
      "orderId": ""
    }
  }
}
```

# TripUpdate

> **MUTATION**

```
mutation tripUpdate($id: Uuid!, $input: TripHdAddDtoInput) {
  tripUpdate(id: $id, input: $input) {
    ...TripDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "vehicleId": "",
    "licensePlate": "",
    "startTrip": "",
    "agentId": "",
    "driverId": "",
    "helperId": "",
    "driverLicens": "",
    "tripDetail": {
      "orderId": ""
    }
  }
}
```

# TripCancel

> **MUTATION**

```
mutation tripCancel($id: Uuid!) {
  tripCancel(id: $id) {
    ...TripDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
