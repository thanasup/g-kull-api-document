[Back to readme](../README.md)

> **SCHEMA**

- [MasterCityAddDtoInput](../schema/inputs/master.md#mastercityadddtoinput)
- [MasterCityUpdateDtoInput](../schema/inputs/master.md#mastercityupdatedtoinput)

# CityCreate

> **MUTATION**

```
mutation CityCreate($input: MasterCityAddDtoInput) {
  cityCreate(input: $input) {
    code
    name
    nameTh
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "code": "",
    "name": "",
    "nameTh": "",
    "countryId": "",
    "provinceId": "",
    "isActive": true
  }
}
```

# CityUpdate

> **MUTATION**

```
mutation CityUpdate($id: Uuid!, $input: MasterCityUpdateDtoInput) {
  cityUpdate(id: $id, input: $input) {
    code
    name
    nameTh
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "code": "",
    "name": "",
    "nameTh": "",
    "countryId": "",
    "provinceId": "",
    "isActive": true
  }
}
```

# CitySoftDelete

> **MUTATION**

```
mutation citySoftDelete ($id: Uuid!) {
  citySoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
