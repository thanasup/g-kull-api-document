[Back to readme](../README.md)

> **SCHEMA**

- [AccountSecurityUpdateDtoInput](../schema/inputs/account.md#accountsecurityupdatedtoinput)
- [role](../schema/inputs/role.md#roleadddtoinput)

# AccountUpdateSecuritySettings

> **MUTATION**

```
mutation AccountUpdateSecuritySettings($input: AccountSecurityUpdateDtoInput) {
  accountSecuritySettings(input: $input) {
    id
    role {
      code
      name
      description
      id
    }
    jwtToken
    userType
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "authId": "",
    "username": "",
    "password": "",
    "confirmPassword": ""
  }
}
```
