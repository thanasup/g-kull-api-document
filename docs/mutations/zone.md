[Back to readme](../README.md)

> **SCHEMA**

- [MasterZoneAddDtoInput](../schema/inputs/master.md#masterzoneadddtoinput)
- [MasterZoneUpdateDtoInput](../schema/inputs/master.md#masterzoneupdatedtoinput)

# ZoneCreate

> **MUTATION**

```
mutation ZoneCreate($input: MasterZoneAddDtoInput) {
  zoneCreate(input: $input) {
    origin
    destination
    code
    name
    description
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "origin": true,
    "destination": true,
    "dropPointType": "",
    "code": "",
    "name": "",
    "description": "",
    "isActive": true
  }
}
```

# ZoneUpdate

> **MUTATION**

```
mutation ZoneUpdate($id: Uuid!, $input: MasterZoneUpdateDtoInput) {
  zoneUpdate(id: $id, input: $input) {
    origin
    destination
    code
    name
    description
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "origin": true,
    "destination": true,
    "dropPointType": "",
    "code": "",
    "name": "",
    "description": "",
    "isActive": true
  }
}
```

# ZoneSoftDelete

> **MUTATION**

```
mutation zoneSoftDelete ($id: Uuid!) {
    zoneSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
