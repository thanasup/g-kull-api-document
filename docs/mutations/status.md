[Back to readme](../README.md)

> **SCHEMA**

- [MasterStatusAddDtoInput](../schema/inputs/master.md#masterstatusadddtoinput)
- [MasterStatusUpdateDtoInput](../schema/inputs/master.md#masterstatusupdatedtoinput)
- [MasterStatusDto](../schema/objects/master.md#masterstatusdto)

# StatusCreate

> **MUTATION**

```
mutation StatusCreate($input: MasterStatusAddDtoInput) {
  statusCreate(input: $input) {
    ...MasterStatusDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "code": "",
    "name": "",
    "photoRequired": true,
    "isRequired": true,
    "isInternal": true,
    "isExternal": true,
    "isActive": true
  }
}
```

# StatusUpdate

> **MUTATION**

```
mutation StatusUpdate($id: Uuid!, $input: MasterStatusUpdateDtoInput) {
  statusUpdate(id: $id, input: $input) {
    ...MasterStatusDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "code": "",
    "name": "",
    "photoRequired": true,
    "isRequired": true,
    "isInternal": true,
    "isExternal": true,
    "isActive": true
  }
}
```

# ServiceSoftDelete

> **MUTATION**

```
mutation statusSoftDelete ($id: Uuid!) {
  statusSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
