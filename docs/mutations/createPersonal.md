# CreatePersonal

[Back to readme](../README.md)

> **SCHEMA**

- [CreatePersonalDtoInput](../schema/inputs/create.md#createpersonaldtoinput)

> **MUTATION**

```
mutation RegisterPersonal($input: CreatePersonalDtoInput) {
  createPersonal(input: $input) {
    ...CustomerDto
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "description": "",
    "title": "",
    "firstName": "",
    "lastName": "",
    "phone": "",
    "tel": "",
    "isActive": true,
    "userType": "",
    "email": "",
    "username": "",
    "password": "",
    "confirmPassword": "",
    "acceptTerms": true,
    "isPersistent": true
  }
}
```
