# Register

[Back to readme](../README.md)

> **SCHEMA**

- [AuthRegisterDtoInput](../schema/inputs/auth.md#authregisterdtoinput)

> **MUTATION**

```
mutation Register($input: AuthRegisterDtoInput) {
  register(input: $input)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "userType": "",
    "email": "",
    "username": "",
    "password": "",
    "confirmPassword": "",
    "acceptTerms": true,
    "isPersistent": true
  }
}
```
