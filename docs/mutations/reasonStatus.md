[Back to readme](../README.md)

> **SCHEMA**

- [MasterReasonStatusAddDtoInput](../schema/inputs/master.md#masterreasonstatusadddtoinput)
- [MasterReasonStatusUpdateDtoInput](../schema/inputs/master.md#masterreasonstatusupdatedtoinput)
- [MasterReasonStatusDto](../schema/objects/master.md#masterreasonstatusdto)

# ReasonStatusCreate

> **MUTATION**

```
mutation ReasonStatusCreate($input: MasterReasonStatusAddDtoInput) {
  reasonStatusCreate(input: $input) {
    ...MasterReasonStatusDto
    masterStatus {
      code
      name
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "code": "",
    "statusId": "",
    "internalReason": "",
    "externalReason": "",
    "isActive": true
  }
}
```

# ReasonStatusUpdate

> **MUTATION**

```
mutation ReasonStatusUpdate($id: Uuid!, $input: MasterReasonStatusUpdateDtoInput) {
  reasonStatusUpdate(id: $id, input: $input) {
    ...MasterReasonStatusDto
    masterStatus {
      code
      name
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "code": "",
    "statusId": "",
    "internalReason": "",
    "externalReason": "",
    "isActive": true
  }
}
```

# ReasonStatusSoftDelete

> **MUTATION**

```
mutation reasonStatusSoftDelete ($id: Uuid!) {
    reasonStatusSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
