[Back to readme](../README.md)

> **SCHEMA**

- [MasterRateAddDtoInput](../schema/inputs/master.md#masterrateadddtoinput)
- [MasterRateUpdateDtoInput](../schema/inputs/master.md#masterrateupdatedtoinput)
- [agent](../schema/inputs/master.md#agentaddDtoInput)
- [vehicle](../schema/inputs/master.md#mastervehicleadddtoinput)
- [zone](../schema/inputs/master.md#masterzoneadddtoinput)

# RateCreate

> **MUTATION**

```
mutation RateCreate($input: MasterRateAddDtoInput) {
  rateCreate(input: $input) {
    agentId
    vehicleId
    zoneIdFrom
    zoneIdTo
    cost
    sale
    agent {
      code
      name
    }
    vehicle {
      code
      name
      description
    }
    zoneFrom {
      origin
      destination
      code
      name
      description
    }
    zoneTo {
      origin
      destination
      code
      name
      description
    }
    description
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "agentId": "",
    "vehicleId": "",
    "zoneIdFrom": "",
    "zoneIdTo": "",
    "cost": "",
    "sale": "",
    "description": "",
    "isActive": true
  }
}
```

# RateUpdate

> **MUTATION**

```
mutation RateUpdate($id: Uuid!, $input: MasterRateUpdateDtoInput) {
  rateUpdate(id: $id, input: $input) {
    agentId
    vehicleId
    zoneIdFrom
    zoneIdTo
    cost
    sale
    agent {
      code
      name
    }
    vehicle {
      code
      name
      description
    }
    zoneFrom {
      origin
      destination
      code
      name
      description
    }
    zoneTo {
      origin
      destination
      code
      name
      description
    }
    description
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "agentId": "",
    "vehicleId": "",
    "zoneIdFrom": "",
    "zoneIdTo": "",
    "cost": "",
    "sale": "",
    "description": "",
    "isActive": true
  }
}
```

# ProvinceSoftDelete

> **MUTATION**

```
mutation rateSoftDelete ($id: Uuid!) {
  rateSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
