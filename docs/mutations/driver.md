[Back to readme](../README.md)

> **SCHEMA**

- [DriverAddDtoInput](../schema/inputs/diiver.md#driveradddtoinput)
- [DriverUpdateDtoInput](../schema/inputs/diiver.md#driverupdatedtoinput)
- [DriverDto](../schema/objects/diiver.md#driverdto)

# DriverCreate

> **MUTATION**

```
mutation DriverCreate($input: DriverAddDtoInput) {
  driverCreate(input: $input) {
    ...DriverDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "idCard": "",
    "drivingLicenseId": "",
    "ability": "",
    "dateOfBirth": "",
    "sex": "",
    "driverType": "",
    "code": "",
    "title": "",
    "firstName": "",
    "lastName": "",
    "phone": "",
    "tel": "",
    "email": "",
    "isActive": true
  }
}
```

# DriverUpdate

> **MUTATION**

```
mutation DriverUpdate($id: Uuid!, $input: DriverUpdateDtoInput) {
  driverUpdate(id: $id, input: $input) {
    ...DriverDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "idCard": "",
    "drivingLicenseId": "",
    "ability": "",
    "dateOfBirth": "",
    "sex": "",
    "driverType": "",
    "code": "",
    "title": "",
    "firstName": "",
    "lastName": "",
    "phone": "",
    "tel": "",
    "email": "",
    "isActive": true
  }
}
```

# DriverSoftDelete

> **MUTATION**

```
mutation DriverSoftDelete ($id: Uuid!) {
    driverSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
