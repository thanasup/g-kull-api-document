[Back to readme](../README.md)

> **SCHEMA**

- [MasterRouteStatusDetailAddDtoInput](../schema/inputs/master.md#masterroutestatusdetailadddtoinput)
- [MasterRouteStatusDetailUpdateDtoInput](../schema/inputs/master.md#masterroutestatusdetailupdatedtoinput)
- [MasterRouteStatusDetailDto](../schema/objects/master.md#masterroutestatusdetaildto)
- [routeStatus](../schema/inputs/master.md#masterroutestatusadddtoinput)
- [masterStatus](../schema/inputs/master.md#masterstatusadddtoinput)

# RouteStatusDetailCreate

> **MUTATION**

```
mutation RouteStatusDetailCreate($input: MasterRouteStatusDetailAddDtoInput) {
  routeStatusDetailCreate(input: $input) {
    ...MasterRouteStatusDetailDto
    routeStatus {
      code
      name: code
    }
    masterStatus {
      code
      name
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "routeStatusId": "",
    "statusId": "",
    "isActive": true
  }
}
```

# RouteStatusDetailUpdate

> **MUTATION**

```
mutation RouteStatusDetailUpdate($id: Uuid!, $input: MasterRouteStatusDetailUpdateDtoInput) {
  routeStatusDetailUpdate(id: $id, input: $input) {
    ...MasterRouteStatusDetailDto
    routeStatus {
      code
      name: code
    }
    masterStatus {
      code
      name
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "routeStatusId": "",
    "statusId": "",
    "isActive": true
  }
}
```

# RouteStatusDetailSoftDelete

> **MUTATION**

```
mutation routeStatusDetailSoftDelete ($id: Uuid!) {
  routeStatusDetailSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
