[Back to readme](../README.md)

> **SCHEMA**

- [OrderTrackingAddDtoInput](../schema/inputs/order.md#ordertrackingadddtoinput)
- [OrderTrackingUpdateDtoInput](../schema/inputs/order.md#ordertrackingupdatedtoinput)
- [OrderTrackingDto](../schema/objects/order.md#ordertrackingdto)

# TrackingCreate

> **MUTATION**

```
mutation TrackingCreate($input: OrderTrackingAddDtoInput) {
  trackingCreate(input: $input) {
    ...OrderTrackingDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "trackingDate": "",
    "orderId": "",
    "statusId": "",
    "reasonId": "",
    "remarks": "",
    "isActive": true
  }
}
```

# TrackingUpdate

> **MUTATION**

```
mutation TrackingUpdate($id: Uuid!, $input: OrderTrackingUpdateDtoInput) {
  trackingUpdate(id: $id, input: $input) {
    ...OrderTrackingDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "trackingDate": "",
    "orderId": "",
    "statusId": "",
    "reasonId": "",
    "remarks": "",
    "isActive": true
  }
}
```

# TrackingSoftDelete

> **MUTATION**

```
mutation trackingSoftDelete ($id: Uuid!) {
    trackingSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
