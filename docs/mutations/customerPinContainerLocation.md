[Back to readme](../README.md)

# CustomerPinContainerLocation

> **MUTATION**

```
mutation CustomerPinContainerLocation ($customerId: Uuid!, $containerLocationId: Uuid!) {
  customerPinContainerLocation (customerId: $customerId, containerLocationId: $containerLocationId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "customerId": "",
  "containerLocationId": ""
}
```

# CustomerUnPinContainerLocation

> **MUTATION**

```
mutation CustomerUnPinContainerLocation ($customerId: Uuid!, $containerLocationId: Uuid!) {
  customerUnPinContainerLocation (customerId: $customerId, containerLocationId: $containerLocationId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "customerId": "",
  "containerLocationId": ""
}
```
