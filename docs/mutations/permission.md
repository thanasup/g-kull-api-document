[Back to readme](../README.md)

> **SCHEMA**

- [PermissionAddDtoInput](../schema/inputs/permission.md#permissionadddtoinput)
- [PermissionDto](../schema/objects/permission.md#permissiondto)

# PermissionCreate

> **MUTATION**

```
mutation permissionCreate($roleId: Uuid!, $input: [PermissionAddDtoInput]) {
  permissionCreate(input: $input, roleId: $roleId) {
    ...PermissionDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "roleId": "",
  "input": {
    "roleId": "",
    "menuId": "",
    "canCreate": true,
    "canView": true,
    "canAmend": true,
    "canDelete": true,
    "canApprove": true,
    "canConfirm": true,
    "canImport": true,
    "canExport": true,
    "canPrint": true,
    "role": {
      "code": "",
      "name": "",
      "description": "",
      "isActive": true
    },
    "masterMenu": {
      "seq": "",
      "parentCode": "",
      "i18n": "",
      "link": "",
      "icon": "",
      "isGroup": true,
      "hideInBreadcrumb": true,
      "code": "",
      "name": "",
      "description": "",
      "isActive": true
    },
    "isActive": true
  }
}
```
