# Logout

[Back to readme](../README.md)

> **MUTATION**

```
mutation Logout {
    logout
}
```

> **GRAPHQL VARIABLES**

```json
{}
```
