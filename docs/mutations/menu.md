[Back to readme](../README.md)

> **SCHEMA**

- [MasterMenuAddDtoInput](../schema/inputs/master.md#mastermenuadddtoinput)
- [MasterMenuUpdateDtoInput](../schema/inputs/master.md#mastermenuupdatedtoinput)
- [MasterMenuDto](../schema/objects/master.md#mastermenudto)

# MenuCreate

> **MUTATION**

```
mutation menuCreate($input: MasterMenuAddDtoInput) {
  menuCreate(input: $input) {
    ...MasterMenuDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "seq": "",
    "parentCode": "",
    "i18n": "",
    "link": "",
    "icon": "",
    "isGroup": true,
    "hideInBreadcrumb": true,
    "code": "",
    "name": "",
    "description": "",
    "isActive": true
  }
}
```

# MenuUpdate

> **MUTATION**

```
mutation menuUpdate($id: Uuid!, $input: MasterMenuUpdateDtoInput) {
  menuUpdate(id: $id, input: $input) {
    ...MasterMenuDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "seq": "",
    "parentCode": "",
    "i18n": "",
    "link": "",
    "icon": "",
    "isGroup": true,
    "hideInBreadcrumb": true,
    "code": "",
    "name": "",
    "description": "",
    "isActive": true
  }
}
```

# MenuSoftDelete

> **MUTATION**

```
mutation menuSoftDelete ($id: Uuid!) {
    menuSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
