[Back to readme](../README.md)

# CustomerPinAddress

> **MUTATION**

```
mutation CustomerPinAddress ($addressId: Uuid!) {
  customerPinAddress (addressId: $addressId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": ""
}
```

# CustomerUnPinAddress

> **MUTATION**

```
mutation CustomerUnPinAddress ($addressId: Uuid!) {
  customerUnPinAddress (addressId: $addressId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": ""
}
```
