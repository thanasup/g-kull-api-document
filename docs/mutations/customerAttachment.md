[Back to readme](../README.md)

> **SCHEMA**

- [CustomerAttachmentAddDtoInput](../schema/inputs/customer.md#customerattachmentadddtoinput)
- [CustomerAttachmentDto](../schema/objects/customer.md#customerattachmentdto)

# CustomerUpdateContact

> **MUTATION**

```
mutation CustomerAddAttachment($customerId: Uuid!, $input: [CustomerAttachmentAddDtoInput]) {
  customerAddAttachment(customerId: $customerId, input: $input) {
    ...CustomerAttachmentDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "customerId": "",
  "input": {
    "type": "",
    "name": "",
    "path": "",
    "attachFile": "",
    "isActive": true
  }
}
```

# CustomerDeleteAttachment

> **MUTATION**

```
mutation CustomerDeleteAttachment ($attachmentId: Uuid!) {
  customerDeleteAttachment (attachmentId: $attachmentId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "attachmentId": ""
}
```
