[Back to readme](../README.md)

> **SCHEMA**

- [AccountBaseUpdateDtoInput](../schema/inputs/account.md#accountbaseupdatedtoinput)
- [role](../schema/inputs/role.md#roleadddtoinput)

# AccountUpdateBaseSettings

> **MUTATION**

```
mutation AccountUpdateBaseSettings($input: AccountBaseUpdateDtoInput) {
  accountBaseSettings(input: $input) {
    fullName
    role {
      code
      name
      description
      id
    }
    email
    isVerified
    verified
    code
    title
    firstName
    lastName
    phone
    isActive
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "authId": "",
    "title": "",
    "firstName": "",
    "lastname": "",
    "phone": "",
    "email": ""
  }
}
```
