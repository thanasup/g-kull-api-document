[Back to readme](../README.md)

> **SCHEMA**

- [MasterServiceAddDtoInput](../schema/inputs/master.md#masterserviceadddtoinput)
- [MasterServiceUpdateDtoInput](../schema/inputs/master.md#masterserviceupdatedtoinput)
- [MasterServiceDto](../schema/objects/master.md#masterservicedto)

# ServiceCreate

> **MUTATION**

```
mutation serviceCreate($input: MasterServiceAddDtoInput) {
  serviceCreate(input: $input) {
    ...MasterServiceDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "free": true,
    "cost": "",
    "sale": "",
    "currency": "",
    "requireAdditional": "",
    "parentCode": "",
    "code": "",
    "name": "",
    "description": "",
    "isActive": true
  }
}
```

# ServiceUpdate

> **MUTATION**

```
mutation serviceUpdate($id: Uuid!, $input: MasterServiceUpdateDtoInput) {
  serviceUpdate(id: $id, input: $input) {
    ...MasterServiceDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "free": true,
    "cost": "",
    "sale": "",
    "currency": "",
    "requireAdditional": "",
    "parentCode": "",
    "code": "",
    "name": "",
    "description": "",
    "isActive": true
  }
}
```

# ServiceSoftDelete

> **MUTATION**

```
mutation serviceSoftDelete ($id: Uuid!) {
  serviceSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
