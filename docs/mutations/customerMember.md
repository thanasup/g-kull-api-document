[Back to readme](../README.md)

> **SCHEMA**

- [CreateMemberDtoInput](../schema/inputs/customer.md#creatememberdtoinput)
- [CustomerMemberUpdateDtoInput](../schema/inputs/customer.md#customermemberupdatedtoinput)
- [CustomerMemberDto](../schema/objects/customer.md#customermemberdto)

# CustomerCreateMember

> **MUTATION**

```
mutation CustomerCreateMember($customerId: Uuid!, $input: CreateMemberDtoInput) {
  customerCreateMember(customerId: $customerId, input: $input) {
    ...CustomerMemberDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "customerId": "",
  "input": {
    "description": "",
    "title": "",
    "firstName": "",
    "lastName": "",
    "phone": "",
    "tel": "",
    "isActive": true,
    "userType": "",
    "email": "",
    "username": "",
    "password": "",
    "confirmPassword": "",
    "acceptTerms": true,
    "isPersistent": true
  }
}
```

# CustomerUpdateMember

> **MUTATION**

```
mutation CustomerUpdateMember($memberId: Uuid!, $input: CustomerMemberUpdateDtoInput) {
  customerUpdateMember(memberId: $memberId, input: $input) {
    ...CustomerMemberDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "memberId": "",
  "input": {
    "title": "",
    "firstName": "",
    "lastName": "",
    "phone": "",
    "tel": "",
    "email": "",
    "isActive": true
  }
}
```

# CustomerSoftDeleteMember

> **MUTATION**

```
mutation CustomerSoftDeleteMember ($memberId: Uuid!) {
  customerSoftDeleteMember (memberId: $memberId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "memberId": ""
}
```

