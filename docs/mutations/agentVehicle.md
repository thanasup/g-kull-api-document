[Back to readme](../README.md)

> **SCHEMA**

- [AgentVehicleAddDtoInput](../schema/inputs/agent.md#agentvehicleadddtoinput)
- [AgentVehicleUpdateDtoInput](../schema/inputs/agent.md#agentvehicleupdatedtoinput)
- [vehicle](../schema/inputs/master.md#mastervehicleadddtoinput)
- [driver](../schema/inputs/driver.md#driveraddDtoinput)

# AgentCreateVehicle

> **MUTATION**

```
mutation AgentCreateVehicle($agentId: Uuid!, $input: AgentVehicleAddDtoInput) {
  agentCreateVehicle(agentId: $agentId, input: $input) {
    caution
    serviceCondition
    agentId
    vehicleId
    license
    driverId
    hasDim
    length
    width
    height
    weight
    maxPayLoad
    maxLiftingWeight
    cbm
    onlyOneArticle
    vehicle {
      freeTime
      hasDim
      length
      width
      height
      weight
      maxPayLoad
      maxLiftingWeight
      cbm
      code
      name
      description
    }
    driver {
      isApprove
      idCard
      drivingLicenseId
      dateOfBirth
      sex
      age
      code
      fullName
      phone
      email
    }
    id
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "agentId": "",
  "input": {
    "caution": "",
    "serviceCondition": "",
    "vehicleId": "",
    "license": "",
    "driverId": "",
    "hasDim": true,
    "length": "",
    "width": "",
    "height": "",
    "weight": "",
    "maxPayLoad": "",
    "maxLiftingWeight": "",
    "onlyOneArticle": true,
    "isActive": true
  }
}
```

# AgentUpdateVehicle

> **MUTATION**

```
mutation AgentUpdateVehicle($vehicleId: Uuid!, $input: AgentVehicleUpdateDtoInput) {
  agentUpdateVehicle(vehicleId: $vehicleId, input: $input) {
    caution
    serviceCondition
    agentId
    vehicleId
    license
    driverId
    hasDim
    length
    width
    height
    weight
    maxPayLoad
    maxLiftingWeight
    cbm
    onlyOneArticle
    vehicle {
      freeTime
      hasDim
      length
      width
      height
      weight
      maxPayLoad
      maxLiftingWeight
      cbm
      code
      name
      description
    }
    driver {
      isApprove
      idCard
      drivingLicenseId
      dateOfBirth
      sex
      age
      code
      fullName
      phone
      email
    }
    id
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "vehicleId": "",
  "input": {
    "caution": "",
    "serviceCondition": "",
    "vehicleId": "",
    "license": "",
    "driverId": "",
    "hasDim": true,
    "length": "",
    "width": "",
    "height": "",
    "weight": "",
    "maxPayLoad": "",
    "maxLiftingWeight": "",
    "onlyOneArticle": true,
    "isActive": true
  }
}
```

# AgentSoftDeleteVehicle

> **MUTATION**

```
mutation AgentSoftDeleteVehicle ($vehicleId: Uuid!) {
  agentSoftDeleteVehicle (vehicleId: $vehicleId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "vehicleId": ""
}
```
