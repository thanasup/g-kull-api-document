# RefreshToken

[Back to readme](../README.md)

> **MUTATION**

```
mutation RefreshToken {
  refreshToken {
    id
    role {
      code
      name
      description
      id
    }
    jwtToken
    userType
  }
}
```

> **GRAPHQL VARIABLES**

```json
{}
```
