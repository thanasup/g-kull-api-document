[Back to readme](../README.md)

> **SCHEMA**

- [CreateEmployeeDtoInput](../schema/inputs/create.md#createemployeedtoinput)
- [EmployeeAddDtoInput](../schema/inputs/employee.md#employeeadddtoinput)
- [EmployeeDto](../schema/objects/employee.md#employeedto)

# EmployeeCreate

> **MUTATION**

```
mutation employeeCreate($input: CreateEmployeeDtoInput) {
  employeeCreate(input: $input) {
    ...EmployeeDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "title": "",
    "firstName": "",
    "lastName": "",
    "phone": "",
    "roleId": "",
    "userType": "",
    "email": "",
    "username": "",
    "password": "",
    "confirmPassword": "",
    "acceptTerms": true,
    "isPersistent": true
  }
}
```

# EmployeeUpdate

> **MUTATION**

```
mutation employeeUpdate($id: Uuid!, $input: EmployeeAddDtoInput) {
  employeeUpdate(id: $id, input: $input) {
    ...EmployeeDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "roleId": "",
    "code": "",
    "title": "",
    "firstName": "",
    "lastName": "",
    "phone": "",
    "tel": "",
    "email": "",
    "isActive": true
  }
}
```

# EmployeeSoftDelete

> **MUTATION**

```
mutation employeeSoftDelete($id: Uuid!) {
  employeeSoftDelete(id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
