[Back to readme](../README.md)

> **SCHEMA**

- [AuthChangePasswordDtoInput](../schema/inputs/auth.md#authchangepassworddtoinput)

# ChangePassword

> **MUTATION**

```
mutation ChangePassword ($input: AuthChangePasswordDtoInput) {
  changePassword (input: $input) {
    id
    role {
      code
      name
      description
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
      id
    }
    jwtToken
    userType
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "id": "",
    "password": "",
    "confirmPassword": ""
  }
}
```
