[Back to readme](../README.md)

> **SCHEMA**

- [OrderPriceAddDtoInput](../schema/inputs/order.md#orderpriceadddtoinput)
- [OrderPriceConfirmApAddDtoInput](../schema/inputs/order.md#orderpriceconfirmapadddtoinput)
- [OrderPriceDto](../schema/objects/order.md#orderpricedto)

# OrderAddOrderPrice

> **MUTATION**

```
mutation orderAddOrderPrice($orderId: Uuid!, $input: [OrderPriceAddDtoInput]) {
  orderAddOrderPrice(orderId: $orderId, input: $input) {
    ...OrderPriceDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "orderId": "",
  "input": {
    "agentId": "",
    "refNo1": "",
    "refNo2": "",
    "priceType": "",
    "name": "",
    "description": "",
    "vatInclude": true,
    "qty": 0,
    "amount": "",
    "vatRate": "",
    "whtRate": "",
    "discountType": "",
    "discount": "",
    "invoiceNo": ""
  }
}
```

# OrderUpdateOrderPrice

> **MUTATION**

```
mutation orderUpdateOrderPrice($orderPriceId: Uuid!, $input: OrderPriceAddDtoInput) {
  orderUpdateOrderPrice(orderPriceId: $orderPriceId, input: $input) {
    ...OrderPriceDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "orderPriceId": "",
  "input": {
    "agentId": "",
    "refNo1": "",
    "refNo2": "",
    "priceType": "",
    "name": "",
    "description": "",
    "vatInclude": true,
    "qty": 0,
    "amount": "",
    "vatRate": "",
    "whtRate": "",
    "discountType": "",
    "discount": "",
    "invoiceNo": ""
  }
}
```

# OrderRemoveOrderPrice

> **MUTATION**

```
mutation orderRemoveOrderPrice ($orderPriceId: Uuid!) {
    orderRemoveOrderPrice (orderPriceId: $orderPriceId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "orderPriceId": ""
}
```

# OrderPriceConfirmAccountPayable

> **MUTATION**

```
mutation orderPriceConfirmAccountPayable($input: [OrderPriceConfirmApAddDtoInput]) {
  orderPriceConfirmAccountPayable(input: $input) {
    ...OrderPriceDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "id": "",
    "invoiceNo": ""
  }
}
```

# OrderPriceCancelAccountPayable

> **MUTATION**

```
mutation orderPriceCancelAccountPayable($orderPriceId: [Uuid!]) {
  orderPriceCancelAccountPayable(orderPriceId: $orderPriceId) {
    ...OrderPriceDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "orderPriceId": [
    ""
  ]
}
```
