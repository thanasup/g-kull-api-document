[Back to readme](../README.md)

> **SCHEMA**

- [AgentAddressAddDtoInput](../schema/inputs/agent.md#agentaddressadddtoinput)
- [AgentAddressUpdateDtoInput](../schema/inputs/agent.md#agentaddressupdatedtoinput)
- [country](../schema/inputs/master.md#mastercountryadddtoinput)
- [province](../schema/inputs/master.md#masterprovinceadddtoinput)
- [city](../schema/inputs/master.md#mastercityadddtoinput)
- [area](../schema/inputs/master.md#masterareaadddtoinput)
- [postalCode](../schema/inputs/master.md#masterpostalcodeadddtoinput)

# AgentCreateAddress

> **MUTATION**

```
mutation AgentCreateAddress($agentId: Uuid!, $input: AgentAddressAddDtoInput) {
  agentCreateAddress(agentId: $agentId, input: $input) {
    isDefault
    addressType
    address
    tel
    phone
    email
    gpsLat
    gpsLon
    country {
      code
      name
      nameTh
    }
    province {
      code
      name
      nameTh
    }
    city {
      code
      name
      nameTh
    }
    area {
      code
      name
      nameTh
    }
    postalCode {
      code
    }
    id
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "agentId": "",
  "input": {
    "isDefault": true,
    "addressType": "",
    "contactName": "",
    "address": "",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "areaId": "",
    "postalCodeId": "",
    "tel": "",
    "phone": "",
    "email": "",
    "gpsLat": "",
    "gpsLon": "",
    "isActive": true
  }
}
```

# AgentUpdateAddress

> **MUTATION**

```
mutation AgentUpdateAddress($addressId: Uuid!, $input: AgentAddressUpdateDtoInput) {
  agentUpdateAddress(addressId: $addressId, input: $input) {
    isDefault
    addressType
    address
    tel
    phone
    email
    gpsLat
    gpsLon
    country {
      code
      name
      nameTh
    }
    province {
      code
      name
      nameTh
    }
    city {
      code
      name
      nameTh
    }
    area {
      code
      name
      nameTh
    }
    postalCode {
      code
    }
    id
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": "",
  "input": {
    "isDefault": true,
    "addressType": "",
    "contactName": "",
    "address": "",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "areaId": "",
    "postalCodeId": "",
    "tel": "",
    "phone": "",
    "email": "",
    "gpsLat": "",
    "gpsLon": "",
    "isActive": true
  }
}
```

# AgentSoftDeleteAddress

> **MUTATION**

```
mutation AgentSoftDeleteAddress ($addressId: Uuid!) {
  agentSoftDeleteAddress (addressId: $addressId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": ""
}
```
