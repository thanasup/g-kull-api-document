[Back to readme](../README.md)

> **SCHEMA**

- [DriverAddressAddDtoInput](../schema/inputs/diiver.md#driveraddressadddtoinput)
- [DriverAddressUpdateDtoInput](../schema/inputs/diiver.md#driveraddressupdatedtoinput)
- [country](../schema/inputs/master.md#mastercountryadddtoinput)
- [province](../schema/inputs/master.md#masterprovinceadddtoinput)
- [city](../schema/inputs/master.md#mastercityadddtoinput)
- [area](../schema/inputs/master.md#masterareaadddtoinput)
- [postalCode](../schema/inputs/master.md#masterpostalcodeadddtoinput)

# DriverCreateAddress

> **MUTATION**

```
mutation DriverCreateAddress($id: Uuid!, $input: DriverAddressAddDtoInput) {
  driverCreateAddress(id: $id, input: $input) {
    driverId
    isDefault
    addressType
    addressTypeList
    address
    countryId
    provinceId
    cityId
    areaId
    postalCodeId
    tel
    phone
    email
    gpsLat
    gpsLon
    country {
      code
      name
      nameTh
    }
    province {
      code
      name
      nameTh
    }
    city {
      code
      name
      nameTh
    }
    area {
      code
      name
      nameTh
    }
    postalCode {
      code
    }
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "isDefault": true,
    "addressType": "",
    "contactName": "",
    "address": "",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "areaId": "",
    "postalCodeId": "",
    "tel": "",
    "phone": "",
    "email": "",
    "gpsLat": "",
    "gpsLon": "",
    "isActive": true
  }
}
```

# DriverUpdateAddress

> **MUTATION**

```
mutation DriverUpdateAddress($addressId: Uuid!, $input: DriverAddressUpdateDtoInput) {
  driverUpdateAddress(addressId: $addressId, input: $input) {
    driverId
    isDefault
    addressType
    addressTypeList
    address
    countryId
    provinceId
    cityId
    areaId
    postalCodeId
    tel
    phone
    email
    gpsLat
    gpsLon
    country {
      code
      name
      nameTh
    }
    province {
      code
      name
      nameTh
    }
    city {
      code
      name
      nameTh
    }
    area {
      code
      name
      nameTh
    }
    postalCode {
      code
    }
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": "",
  "input": {
    "isDefault": true,
    "addressType": "",
    "contactName": "",
    "address": "",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "areaId": "",
    "postalCodeId": "",
    "tel": "",
    "phone": "",
    "email": "",
    "gpsLat": "",
    "gpsLon": "",
    "isActive": true
  }
}
```

# DriverSoftDeleteAddress

> **MUTATION**

```
mutation DriverSoftDeleteAddress ($addressId: Uuid!) {
  driverSoftDeleteAddress (addressId: $addressId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": ""
}
```
