[Back to readme](../README.md)

> **SCHEMA**

- [CustomerContactAddDtoInput](../schema/inputs/customer.md#customercontactadddtoinput)
- [CustomerContactUpdateDtoInput](../schema/inputs/customer.md#customercontactupdatedtoinput)
- [CustomerContactDto](../schema/objects/customer.md#customercontactdto)

# CustomerCreateContact

> **MUTATION**

```
mutation CustomerCreateContact($customerId: Uuid!, $input: CustomerContactAddDtoInput) {
  customerCreateContact(customerId: $customerId, input: $input) {
    ...CustomerContactDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "customerId": "",
  "input": {
    "name": "",
    "position": "",
    "phone": "",
    "email": "",
    "contactType": "",
    "isActive": true
  }
}
```

# CustomerUpdateContact

> **MUTATION**

```
mutation CustomerUpdateContact($contactId: Uuid!, $input: CustomerContactUpdateDtoInput) {
  customerUpdateContact(contactId: $contactId, input: $input) {
    ...CustomerContactDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "contactId": "",
  "input": {
    "name": "",
    "position": "",
    "phone": "",
    "email": "",
    "contactType": "",
    "isActive": true
  }
}
```

# CustomerSoftDeleteContact

> **MUTATION**

```
mutation CustomerSoftDeleteContact ($contactId: Uuid!) {
  customerSoftDeleteContact (contactId: $contactId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "contactId": ""
}
```

