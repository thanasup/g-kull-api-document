# VerifyEmail

[Back to readme](../README.md)

> **SCHEMA**

- [VerifyEmailDtoInput](../schema/inputs/auth.md#verifyemaildtoinput)

> **MUTATION**

```
mutation VerifyEmail ($input: VerifyEmailDtoInput) {
  verifyEmail (input: $input)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "token": ""
  }
}
```
