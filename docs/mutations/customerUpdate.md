# CustomerUpdate

[Back to readme](../README.md)

> **SCHEMA**

- [CustomerUpdateDtoInput](../schema/inputs/customer.md#customerupdatedtoinput)
- [CustomerDto](../schema/objects/customer.md#customerdto)

> **MUTATION**

```
mutation CustomerUpdate($id: Uuid!, $input: CustomerUpdateDtoInput) {
  customerUpdate(id: $id, input: $input) {
    ...CustomerDto
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "companyName": "",
    "businessType": "",
    "branch": "",
    "taxId": "",
    "contactName": "",
    "creditLimit": "",
    "einvoiceAndReceipt": true,
    "withholding": "",
    "creditTermMethod": "",
    "creditTermByDate": 0,
    "creditTermByDay": 0,
    "receiveInvoiceMethod": "",
    "receiveInvoiceBetweenDate": "",
    "paymentMethod": "",
    "invoicePlacementDate": "",
    "remittanceEmail": "",
    "billingCycle": "",
    "billingRemark": "",
    "description": "",
    "isActivate": true,
    "title": "",
    "firstName": "",
    "lastName": "",
    "phone": "",
    "tel": "",
    "email": "",
    "isActive": true
  }
}
```
