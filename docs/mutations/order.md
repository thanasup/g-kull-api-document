[Back to readme](../README.md)

> **SCHEMA**

- [OrderAddDtoInput](../schema/inputs/order.md#orderadddtoinput)
- [OrderDto](../schema/objects/order.md#orderdto)
- [OrderProveOfDeliveryAddDtoInput](../schema/inputs/order.md#orderproveofdeliveryadddtoinput)
- [OrderProveOfDeliveryDto](../schema/objects/order.md#orderproveofdeliverydto)

# OrderCreate

> **MUTATION**

```
mutation orderCreate($input: OrderAddDtoInput) {
  orderCreate(input: $input) {
    ...OrderDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "shipmentType": "",
    "requestPickupDate": "",
    "customerType": "",
    "couponCode": "",
    "vehicleId": "",
    "driverId": "",
    "routeStatusId": "",
    "customerId": "",
    "projectId": "",
    "invoiceNo": "",
    "reciptNo": "",
    "refNo1": "",
    "refNo2": "",
    "description": "",
    "currency": "",
    "agentId": "",
    "orderAddress": {
      "points": 0,
      "addressDesc": "",
      "containerLocationId": "",
      "estimateDate": "",
      "free": true,
      "addContact": true,
      "isDefault": true,
      "contactName": "",
      "address": "",
      "countryId": "",
      "provinceId": "",
      "cityId": "",
      "areaId": "",
      "postalCodeId": "",
      "tel": "",
      "phone": "",
      "email": "",
      "gpsLat": "",
      "gpsLon": "",
      "isActive": true
    },
    "orderAdditionalService": {
      "serviceId": "",
      "qty": 0
    },
    "podDescription": ""
  }
}
```

# OrderUpdate

> **MUTATION**

```
mutation orderUpdate($id: Uuid!, $input: OrderAddDtoInput) {
  orderUpdate(id: $id, input: $input) {
    ...OrderDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "shipmentType": "",
    "requestPickupDate": "",
    "customerType": "",
    "couponCode": "",
    "vehicleId": "",
    "driverId": "",
    "routeStatusId": "",
    "customerId": "",
    "projectId": "",
    "invoiceNo": "",
    "reciptNo": "",
    "refNo1": "",
    "refNo2": "",
    "description": "",
    "currency": "",
    "agentId": "",
    "orderAddress": {
      "points": 0,
      "addressDesc": "",
      "containerLocationId": "",
      "estimateDate": "",
      "free": true,
      "addContact": true,
      "isDefault": true,
      "contactName": "",
      "address": "",
      "countryId": "",
      "provinceId": "",
      "cityId": "",
      "areaId": "",
      "postalCodeId": "",
      "tel": "",
      "phone": "",
      "email": "",
      "gpsLat": "",
      "gpsLon": "",
      "isActive": true
    },
    "orderAdditionalService": {
      "serviceId": "",
      "qty": 0
    },
    "podDescription": ""
  }
}
```

# OrderPendingConfirm

> **MUTATION**

```
mutation orderPendingConfirm($id: Uuid!, $input: OrderAddDtoInput) {
  orderPendingConfirm(id: $id, input: $input) {
    ...OrderDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "shipmentType": "",
    "requestPickupDate": "",
    "customerType": "",
    "couponCode": "",
    "vehicleId": "",
    "driverId": "",
    "routeStatusId": "",
    "customerId": "",
    "projectId": "",
    "invoiceNo": "",
    "reciptNo": "",
    "refNo1": "",
    "refNo2": "",
    "description": "",
    "currency": "",
    "agentId": "",
    "orderAddress": {
      "points": 0,
      "addressDesc": "",
      "containerLocationId": "",
      "estimateDate": "",
      "free": true,
      "addContact": true,
      "isDefault": true,
      "contactName": "",
      "address": "",
      "countryId": "",
      "provinceId": "",
      "cityId": "",
      "areaId": "",
      "postalCodeId": "",
      "tel": "",
      "phone": "",
      "email": "",
      "gpsLat": "",
      "gpsLon": "",
      "isActive": true
    },
    "orderAdditionalService": {
      "serviceId": "",
      "qty": 0
    },
    "podDescription": ""
  }
}
```

# OrderConfirm

> **MUTATION**

```
mutation orderConfirm($id: Uuid!, $input: OrderAddDtoInput) {
  orderConfirm(id: $id, input: $input) {
    ...OrderDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "shipmentType": "",
    "requestPickupDate": "",
    "customerType": "",
    "couponCode": "",
    "vehicleId": "",
    "driverId": "",
    "routeStatusId": "",
    "customerId": "",
    "projectId": "",
    "invoiceNo": "",
    "reciptNo": "",
    "refNo1": "",
    "refNo2": "",
    "description": "",
    "currency": "",
    "agentId": "",
    "orderAddress": {
      "points": 0,
      "addressDesc": "",
      "containerLocationId": "",
      "estimateDate": "",
      "free": true,
      "addContact": true,
      "isDefault": true,
      "contactName": "",
      "address": "",
      "countryId": "",
      "provinceId": "",
      "cityId": "",
      "areaId": "",
      "postalCodeId": "",
      "tel": "",
      "phone": "",
      "email": "",
      "gpsLat": "",
      "gpsLon": "",
      "isActive": true
    },
    "orderAdditionalService": {
      "serviceId": "",
      "qty": 0
    },
    "podDescription": ""
  }
}
```

# OrderOnProcess

> **MUTATION**

```
mutation orderOnProcess($id: Uuid!) {
  orderOnProcess(id: $id) {
    ...OrderDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```

# OrderClose

> **MUTATION**

```
mutation orderClose($id: Uuid!) {
  orderClose(id: $id) {
    ...OrderDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```

# OrderCancel

> **MUTATION**

```
mutation orderCancel($id: Uuid!) {
  orderCancel(id: $id) {
    ...OrderDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```

# OrderFail

> **MUTATION**

```
mutation orderFail($id: Uuid!) {
  orderFail(id: $id) {
    ...OrderDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```

# OrderAddAttachment

> **MUTATION**

```
mutation orderAddAttachment($orderId: Uuid!, $input: OrderProveOfDeliveryAddDtoInput) {
  orderAddAttachment(orderId: $orderId, input: $input) {
    ...OrderProveOfDeliveryDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "orderId": "",
  "input": {
    "podDescription": "",
    "attachment": {
      "type": "",
      "name": "",
      "path": "",
      "attachFile": ""
    }
  }
}
```

# OrderRemoveAttachment

> **MUTATION**

```
mutation orderRemoveAttachment ($attachmentId: Uuid!) {
  orderRemoveAttachment (attachmentId: $attachmentId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "attachmentId": ""
}
```
