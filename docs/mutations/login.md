# Login

[Back to readme](../README.md)

> **SCHEMA**

- [AuthLoginDtoInput](../schema/inputs/auth.md#authlogindtoinput)

> **MUTATION**

```
mutation Login($input: AuthLoginDtoInput) {
  login(input: $input) {
    id
    role {
      code
      name
      description
      id
    }
    jwtToken
    userType
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "username": "",
    "password": "",
    "isPersistent": true
  }
}
```
