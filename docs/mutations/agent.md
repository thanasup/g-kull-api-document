[Back to readme](../README.md)

> **SCHEMA**

- [AgentAddDtoInput](../schema/inputs/agent.md#agentadddtoinput)
- [AgentUpdateDtoInput](../schema/inputs/agent.md#agentupdatedtoinput)

# AgentCreate

> **MUTATION**

```
mutation AgentCreate($input: AgentAddDtoInput) {
  agentCreate(input: $input) {
    taxId
    code
    name
    description
    id
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "taxId": "",
    "code": "",
    "name": "",
    "description": "",
    "isActive": true
  }
}
```

# AgentUpdate

> **MUTATION**

```
mutation AgentUpdate($id: Uuid!, $input: AgentUpdateDtoInput) {
  agentUpdate(id: $id, input: $input) {
    taxId
    code
    name
    description
    id
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "taxId": "",
    "code": "",
    "name": "",
    "description": "",
    "isActive": true
  }
}
```

# AgentSoftDelete

> **MUTATION**

```
mutation AgentSoftDelete($id: Uuid!) {
  agentSoftDelete(id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
