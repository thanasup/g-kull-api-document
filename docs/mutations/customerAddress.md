[Back to readme](../README.md)

> **SCHEMA**

- [CustomerAddressAddDtoInput](../schema/inputs/customer.md#customeraddressadddtoinput)
- [CustomerAddressUpdateDtoInput](../schema/inputs/customer.md#customeraddressupdatedtoinput)
- [CustomerAddressDto](../schema/objects/customer.md#customeraddressdto)

# CustomerCreateAddress

> **MUTATION**

```
mutation CustomerCreateAddress($customerId: Uuid!, $input: CustomerAddressAddDtoInput) {
  customerCreateAddress(customerId: $customerId, input: $input) {
    ...CustomerAddressDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "companyName": "",
    "businessType": "",
    "branch": "",
    "taxId": "",
    "contactName": "",
    "creditLimit": "",
    "einvoiceAndReceipt": true,
    "withholding": "",
    "creditTermMethod": "",
    "creditTermByDate": 0,
    "creditTermByDay": 0,
    "receiveInvoiceMethod": "",
    "receiveInvoiceBetweenDate": "",
    "paymentMethod": "",
    "invoicePlacementDate": "",
    "remittanceEmail": "",
    "billingCycle": "",
    "billingRemark": "",
    "description": "",
    "isActivate": true,
    "title": "",
    "firstName": "",
    "lastName": "",
    "phone": "",
    "tel": "",
    "email": "",
    "isActive": true
  }
}
```

# CustomerUpdateAddress

> **MUTATION**

```
mutation CustomerUpdateAddress($addressId: Uuid!, $input: CustomerAddressUpdateDtoInput) {
  customerUpdateAddress(addressId: $addressId, input: $input) {
    ...CustomerAddressDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": "",
  "input": {
    "isActive": true,
    "isDefault": true,
    "addressType": "",
    "contactName": "",
    "address": "",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "areaId": "",
    "postalCodeId": "",
    "tel": "",
    "phone": "",
    "email": "",
    "gpsLat": "",
    "gpsLon": ""
  }
}
```

# CustomerSoftDeleteAddress

> **MUTATION**

```
mutation CustomerSoftDeleteAddress ($addressId: Uuid!) {
  customerSoftDeleteAddress (addressId: $addressId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": ""
}
```

