[Back to readme](../README.md)

> **SCHEMA**

- [CustomerProjectAddDtoInput](../schema/inputs/customer.md#customerprojectadddtoinput)
- [CustomerProjectUpdateDtoInput](../schema/inputs/customer.md#customerprojectupdatedtoinput)
- [CustomerProjectDto](../schema/objects/customer.md#customerprojectdto)

# CustomerCreateProject

> **MUTATION**

```
mutation CustomerCreateProject($customerId: Uuid!, $input: CustomerProjectAddDtoInput) {
  customerCreateProject(customerId: $customerId, input: $input) {
    ...CustomerProjectDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "customerId": "",
  "input": {
    "code": "",
    "name": "",
    "description": "",
    "isActive": true
  }
}
```

# CustomerUpdateProject

> **MUTATION**

```
mutation CustomerUpdateProject($projectId: Uuid!, $input: CustomerProjectUpdateDtoInput) {
  customerUpdateProject(projectId: $projectId, input: $input) {
    ...CustomerProjectDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "projectId": "",
  "input": {
    "code": "",
    "name": "",
    "description": "",
    "isActive": true
  }
}
```

# CustomerSoftDeleteProject

> **MUTATION**

```
mutation CustomerSoftDeleteProject ($projectId: Uuid!) {
    customerSoftDeleteProject (projectId: $projectId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "projectId": ""
}
```
