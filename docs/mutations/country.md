[Back to readme](../README.md)

> **SCHEMA**

- [MasterCountryAddDtoInput](../schema/inputs/master.md#mastercountryadddtoinput)
- [MasterCountryUpdateDtoInput](../schema/inputs/master.md#mastercountryupdatedtoinput)

# CountryCreate

> **MUTATION**

```
mutation CountryCreate($input: MasterCountryAddDtoInput) {
  countryCreate(input: $input) {
    code
    name
    nameTh
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "code": "",
    "name": "",
    "nameTh": "",
    "isActive": true
  }
}
```

# CountryUpdate

> **MUTATION**

```
mutation CountryUpdate($id: Uuid!, $input: MasterCountryUpdateDtoInput) {
  countryUpdate(id: $id, input: $input) {
    code
    name
    nameTh
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "code": "",
    "name": "",
    "nameTh": "",
    "isActive": true
  }
}
```

# CountrySoftDelete

> **MUTATION**

```
mutation countrySoftDelete ($id: Uuid!) {
  countrySoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
