[Back to readme](../README.md)

> **SCHEMA**

- [MasterAreaAddDtoInput](../schema/inputs/master.md#masterareaadddtoinput)
- [MasterAreaUpdateDtoInput](../schema/inputs/master.md#masterareaupdatedtoinput)

# AreaCreate

> **MUTATION**

```
mutation AreaCreate($input: MasterAreaAddDtoInput) {
  areaCreate(input: $input) {
    code
    name
    nameTh
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "code": "",
    "name": "",
    "nameTh": "",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "isActive": true
  }
}
```

# AreaUpdate

> **MUTATION**

```
mutation AreaUpdate($id: Uuid!, $input: MasterAreaUpdateDtoInput) {
  areaUpdate(id: $id, input: $input) {
    code
    name
    nameTh
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "code": "",
    "name": "",
    "nameTh": "",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "isActive": true
  }
}
```

# AreaSoftDelete

> **MUTATION**

```
mutation areaSoftDelete ($id: Uuid!) {
  areaSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
