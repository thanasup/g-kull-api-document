[Back to readme](../README.md)

# CustomerPinDriver

> **MUTATION**

```
mutation CustomerPinDriver ($customerId: Uuid!, $driverId: Uuid!) {
  customerPinDriver (customerId: $customerId, driverId: $driverId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "customerId": "",
  "driverId": ""
}
```

# CustomerUnPinDriver

> **MUTATION**

```
mutation CustomerUnPinDriver ($customerId: Uuid!, $driverId: Uuid!) {
  customerUnPinDriver (customerId: $customerId, driverId: $driverId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "customerId": "",
  "driverId": ""
}
```
