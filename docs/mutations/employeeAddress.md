[Back to readme](../README.md)

> **SCHEMA**

- [EmployeeAddressAddDtoInput](../schema/inputs/create.md#employeeaddressadddtoinput)
- [EmployeeAddressUpdateDtoInput](../schema/inputs/create.md#employeeaddressupdatedtoinput)

# EmployeeCreateAddress

> **MUTATION**

```
mutation employeeCreateAddress($employeeId: Uuid!, $input: EmployeeAddressAddDtoInput) {
  employeeCreateAddress(employeeId: $employeeId, input: $input) {
    employeeId
    isDefault
    addressType
    addressTypeList
    address
    countryId
    provinceId
    cityId
    areaId
    postalCodeId
    tel
    phone
    email
    gpsLat
    gpsLon
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "employeeId": "",
  "input": {
    "isDefault": true,
    "addressType": "",
    "contactName": "",
    "address": "",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "areaId": "",
    "postalCodeId": "",
    "tel": "",
    "phone": "",
    "email": "",
    "gpsLat": "",
    "gpsLon": "",
    "isActive": true
  }
}
```

# EmployeeUpdateAddress

> **MUTATION**

```
mutation employeeUpdateAddress($addressId: Uuid!, $input: EmployeeAddressUpdateDtoInput) {
  employeeUpdateAddress(addressId: $addressId, input: $input) {
    employeeId
    isDefault
    addressType
    addressTypeList
    address
    countryId
    provinceId
    cityId
    areaId
    postalCodeId
    tel
    phone
    email
    gpsLat
    gpsLon
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": "",
  "input": {
    "isDefault": true,
    "addressType": "",
    "contactName": "",
    "address": "",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "areaId": "",
    "postalCodeId": "",
    "tel": "",
    "phone": "",
    "email": "",
    "gpsLat": "",
    "gpsLon": "",
    "isActive": true
  }
}
```

# EmployeeSoftDeleteAddress

> **MUTATION**

```
mutation employeeSoftDeleteAddress ($addressId: Uuid!) {
  employeeSoftDeleteAddress (addressId: $addressId)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": ""
}
```
