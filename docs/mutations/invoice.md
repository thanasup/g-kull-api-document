[Back to readme](../README.md)

> **SCHEMA**

- [InvoiceHdAddDtoInput](../schema/objects/invoice.md#invoicehdadddtoinput)
- [InvoiceHdDto](../schema/objects/invoice.md#invoicehddto)

# InvoiceCreate

> **MUTATION**

```
mutation invoiceCreate($input: InvoiceHdAddDtoInput) {
  invoiceCreate(input: $input) {
    ...InvoiceHdDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "customerId": "",
    "taxId": "",
    "billingAddress": "",
    "currency": "",
    "crTerm": 0,
    "dueDate": "",
    "invoiceDetail": {
      "orderId": "",
      "orderPriceId": ""
    }
  }
}
```

# InvoiceUpdate

> **MUTATION**

```
mutation invoiceUpdate($id: Uuid!, $input: InvoiceHdAddDtoInput) {
  invoiceUpdate(id: $id, input: $input) {
    ...InvoiceHdDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "customerId": "",
    "taxId": "",
    "billingAddress": "",
    "currency": "",
    "crTerm": 0,
    "dueDate": "",
    "invoiceDetail": {
      "orderId": "",
      "orderPriceId": ""
    }
  }
}
```

# InvoiceCancle

> **MUTATION**

```
mutation invoiceCancel($id: Uuid!) {
  invoiceCancel(id: $id) {
    ...InvoiceHdDto
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
