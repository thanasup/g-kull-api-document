# CreateCompany

[Back to readme](../README.md)

> **SCHEMA**

- [CreateCompanyDtoInput](../schema/inputs/create.md#createcompanydtoinput)

> **MUTATION**

```
mutation CreateCompany($input: CreateCompanyDtoInput) {
  createCompany(input: $input) {
    ...CustomerDto
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "companyName": "",
    "businessType": "",
    "branch": "",
    "taxId": "",
    "email": "",
    "tel": "",
    "contactName": "",
    "description": ""
  }
}
```
