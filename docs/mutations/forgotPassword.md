# ForgotPassword

[Back to readme](../README.md)

> **SCHEMA**

- [AuthForgotPasswordDtoInput](../schema/inputs/auth.md#authforgotpassworddtoinput)

> **MUTATION**

```
mutation ForgotPassword ($input: AuthForgotPasswordDtoInput) {
  forgotPassword (input: $input)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "email": ""
  }
}
```
