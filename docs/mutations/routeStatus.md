[Back to readme](../README.md)

> **SCHEMA**

- [MasterRouteStatusAddDtoInput](../schema/inputs/master.md#masterrouteStatusadddtoinput)
- [MasterRouteStatusUpdateDtoInput](../schema/inputs/master.md#masterroutestatusupdatedtoinput)
- [MasterRouteStatusDto](../schema/objects/master.md#masterroutestatusdto)
- [customer](../schema/inputs/customer.md#customerupdatedtoinput)
- [customerProject](../schema/inputs/customer.md#customerprojectadddtoinput)

# RouteStatusCreate

> **MUTATION**

```
mutation RouteStatusCreate($input: MasterRouteStatusAddDtoInput) {
  routeStatusCreate(input: $input) {
    ...MasterRouteStatusDto
    customer {
      code
      name: companyName
    }
    customerProject {
      code
      name
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "code": "",
    "customerId": "",
    "projectId": "",
    "isActive": true
  }
}
```

# RouteStatusUpdate

> **MUTATION**

```
mutation RouteStatusUpdate($id: Uuid!, $input: MasterRouteStatusUpdateDtoInput) {
  routeStatusUpdate(id: $id, input: $input) {
    ...MasterRouteStatusDto
    customer {
      code
      name: companyName
    }
    customerProject {
      code
      name
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "code": "",
    "customerId": "",
    "projectId": "",
    "isActive": true
  }
}
```

# RouteStatusSoftDelete

> **MUTATION**

```
mutation routeStatusSoftDelete ($id: Uuid!) {
  routeStatusSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
