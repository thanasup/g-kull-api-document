[Back to readme](../README.md)

> **SCHEMA**

- [MasterProvinceAddDtoInput](../schema/inputs/master.md#masterprovinceadddtoinput)
- [MasterProvinceUpdateDtoInput](../schema/inputs/master.md#masterprovinceupdatedtoinput)

# ProvinceCreate

> **MUTATION**

```
mutation ProvinceCreate($input: MasterProvinceAddDtoInput) {
  provinceCreate(input: $input) {
    code
    name
    nameTh
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "code": "",
    "name": "",
    "nameTh": "",
    "countryId": "",
    "isActive": true
  }
}
```

# ProvinceUpdate

> **MUTATION**

```
mutation ProvinceUpdate($id: Uuid!, $input: MasterProvinceUpdateDtoInput) {
  provinceUpdate(id: $id, input: $input) {
    code
    name
    nameTh
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "code": "",
    "name": "",
    "nameTh": "",
    "countryId": "",
    "isActive": true
  }
}
```

# ProvinceSoftDelete

> **MUTATION**

```
mutation provinceSoftDelete ($id: Uuid!) {
  provinceSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
