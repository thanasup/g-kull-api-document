[Back to readme](../README.md)

> **SCHEMA**

- [MasterZoneDetailAddDtoInput](../schema/inputs/master.md#masterzonedetailadddtoinput)
- [MasterZoneDetailUpdateDtoInput](../schema/inputs/master.md#masterzonedetailupdatedtoinput)
- [zone](../schema/inputs/master.md#masterzoneadddtoinput)
- [country](../schema/inputs/master.md#mastercountryadddtoinput)
- [province](../schema/inputs/master.md#masterprovinceadddtoinput)
- [city](../schema/inputs/master.md#mastercityadddtoinput)
- [area](../schema/inputs/master.md#masterareaadddtoinput)
- [postalCode](../schema/inputs/master.md#masterpostalcodeadddtoinput)

# ZoneDetailCreate

> **MUTATION**

```
mutation ZoneDetailCreate($input: MasterZoneDetailAddDtoInput) {
  zoneDetailCreate(input: $input) {
    zoneId
    countryId
    provinceId
    cityId
    areaId
    postalCodeId
    zone {
      origin
      destination
      code
      name
      description
    }
    country {
      code
      name
      nameTh
    }
    province {
      code
      name
      nameTh
    }
    city {
      code
      name
      nameTh
    }
    area {
      code
      name
      nameTh
    }
    postalCode {
      code
    }
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "zoneId": "",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "areaId": "",
    "postalCodeId": "",
    "isActive": true
  }
}
```

# ZoneDetailUpdate

> **MUTATION**

```
mutation ZoneDetailUpdate($id: Uuid!, $input: MasterZoneDetailUpdateDtoInput) {
  zoneDetailUpdate(id: $id, input: $input) {
    zoneId
    countryId
    provinceId
    cityId
    areaId
    postalCodeId
    zone {
      origin
      destination
      code
      name
      description
    }
    country {
      code
      name
      nameTh
    }
    province {
      code
      name
      nameTh
    }
    city {
      code
      name
      nameTh
    }
    area {
      code
      name
      nameTh
    }
    postalCode {
      code
    }
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "zoneId": "",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "areaId": "",
    "postalCodeId": "",
    "isActive": true
  }
}
```

# ZoneDetailSoftDelete

> **MUTATION**

```
mutation zoneDetailSoftDelete ($id: Uuid!) {
    zoneDetailSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
