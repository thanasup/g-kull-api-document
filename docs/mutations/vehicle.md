[Back to readme](../README.md)

> **SCHEMA**

- [MasterVehicleAddDtoInput](../schema/inputs/master.md#mastervehicleadddtoinput)
- [MasterVehicleUpdateDtoInput](../schema/inputs/master.md#mastervehicleupdatedtoinput)
- [MasterVehicleDto](../schema/objects/master.md#mastervehicledto)

# VehicleCreate

> **MUTATION**

```
mutation VehicleCreate($input: MasterVehicleAddDtoInput) {
  vehicleCreate(input: $input) {
    ...MasterVehicleDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "freeTime": "",
    "hasDim": true,
    "length": "",
    "width": "",
    "height": "",
    "weight": "",
    "maxPayLoad": "",
    "maxLiftingWeight": "",
    "vehicleTypeId": "",
    "cost": "",
    "sale": "",
    "currency": "",
    "code": "",
    "name": "",
    "description": "",
    "isActive": true
  }
}
```

# VehicleUpdate

> **MUTATION**

```
mutation vehicleUpdate($id: Uuid!, $input: MasterVehicleUpdateDtoInput) {
  vehicleUpdate(id: $id, input: $input) {
    ...MasterVehicleDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "freeTime": "",
    "hasDim": true,
    "length": "",
    "width": "",
    "height": "",
    "weight": "",
    "maxPayLoad": "",
    "maxLiftingWeight": "",
    "vehicleTypeId": "",
    "cost": "",
    "sale": "",
    "currency": "",
    "code": "",
    "name": "",
    "description": "",
    "isActive": true
  }
}
```

# ServiceSoftDelete

> **MUTATION**

```
mutation vehicleSoftDelete ($id: Uuid!) {
  vehicleSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
