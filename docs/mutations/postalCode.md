[Back to readme](../README.md)

> **SCHEMA**

- [MasterPostalCodeAddDtoInput](../schema/inputs/master.md#masterpostalcodeadddtoinput)
- [MasterPostalCodeUpdateDtoInput](../schema/inputs/master.md#masterpostalcodeupdatedtoinput)

# PostalCodeCreate

> **MUTATION**

```
mutation PostalCodeCreate($input: MasterPostalCodeAddDtoInput) {
  postalCodeCreate(input: $input) {
    code
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "areaId": "",
    "code": "",
    "isActive": true
  }
}
```

# PostalCodeUpdate

> **MUTATION**

```
mutation PostalCodeUpdate($id: Uuid!, $input: MasterPostalCodeUpdateDtoInput) {
  postalCodeUpdate(id: $id, input: $input) {
    code
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": "",
  "input": {
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "areaId": "",
    "code": "",
    "isActive": true
  }
}
```

# PostalCodeSoftDelete

> **MUTATION**

```
mutation postalCodeSoftDelete ($id: Uuid!) {
  postalCodeSoftDelete (id: $id)
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
