### ListFilterInputTypeOfAgentAddressDtoFilterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>all</strong></td>
<td valign="top"><a href="#agentaddressdtofilterinput">AgentAddressDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>none</strong></td>
<td valign="top"><a href="#agentaddressdtofilterinput">AgentAddressDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>some</strong></td>
<td valign="top"><a href="#agentaddressdtofilterinput">AgentAddressDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>any</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### ListFilterInputTypeOfAgentVehicleDtoFilterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>all</strong></td>
<td valign="top"><a href="#agentvehicledtofilterinput">AgentVehicleDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>none</strong></td>
<td valign="top"><a href="#agentvehicledtofilterinput">AgentVehicleDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>some</strong></td>
<td valign="top"><a href="#agentvehicledtofilterinput">AgentVehicleDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>any</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### ListFilterInputTypeOfDriverAddressDtoFilterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>all</strong></td>
<td valign="top"><a href="#driveraddressdtofilterinput">DriverAddressDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>none</strong></td>
<td valign="top"><a href="#driveraddressdtofilterinput">DriverAddressDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>some</strong></td>
<td valign="top"><a href="#driveraddressdtofilterinput">DriverAddressDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>any</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### ListFilterInputTypeOfDriverVehicleDtoFilterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>all</strong></td>
<td valign="top"><a href="#drivervehicledtofilterinput">DriverVehicleDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>none</strong></td>
<td valign="top"><a href="#drivervehicledtofilterinput">DriverVehicleDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>some</strong></td>
<td valign="top"><a href="#drivervehicledtofilterinput">DriverVehicleDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>any</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### ListFilterInputTypeOfInvoiceDtDtoFilterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>all</strong></td>
<td valign="top"><a href="#invoicedtdtofilterinput">InvoiceDtDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>none</strong></td>
<td valign="top"><a href="#invoicedtdtofilterinput">InvoiceDtDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>some</strong></td>
<td valign="top"><a href="#invoicedtdtofilterinput">InvoiceDtDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>any</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### ListFilterInputTypeOfMasterServiceDtoFilterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>all</strong></td>
<td valign="top"><a href="#masterservicedtofilterinput">MasterServiceDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>none</strong></td>
<td valign="top"><a href="#masterservicedtofilterinput">MasterServiceDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>some</strong></td>
<td valign="top"><a href="#masterservicedtofilterinput">MasterServiceDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>any</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### ListFilterInputTypeOfOrderAdditionalServiceDtoFilterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>all</strong></td>
<td valign="top"><a href="#orderadditionalservicedtofilterinput">OrderAdditionalServiceDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>none</strong></td>
<td valign="top"><a href="#orderadditionalservicedtofilterinput">OrderAdditionalServiceDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>some</strong></td>
<td valign="top"><a href="#orderadditionalservicedtofilterinput">OrderAdditionalServiceDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>any</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### ListFilterInputTypeOfOrderAddressDtoFilterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>all</strong></td>
<td valign="top"><a href="#orderaddressdtofilterinput">OrderAddressDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>none</strong></td>
<td valign="top"><a href="#orderaddressdtofilterinput">OrderAddressDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>some</strong></td>
<td valign="top"><a href="#orderaddressdtofilterinput">OrderAddressDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>any</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### ListFilterInputTypeOfOrderPriceDtoFilterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>all</strong></td>
<td valign="top"><a href="#orderpricedtofilterinput">OrderPriceDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>none</strong></td>
<td valign="top"><a href="#orderpricedtofilterinput">OrderPriceDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>some</strong></td>
<td valign="top"><a href="#orderpricedtofilterinput">OrderPriceDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>any</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### ListFilterInputTypeOfOrderRateDtoFilterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>all</strong></td>
<td valign="top"><a href="#orderratedtofilterinput">OrderRateDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>none</strong></td>
<td valign="top"><a href="#orderratedtofilterinput">OrderRateDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>some</strong></td>
<td valign="top"><a href="#orderratedtofilterinput">OrderRateDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>any</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### ListFilterInputTypeOfTripDtDtoFilterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>all</strong></td>
<td valign="top"><a href="#tripdtdtofilterinput">TripDtDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>none</strong></td>
<td valign="top"><a href="#tripdtdtofilterinput">TripDtDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>some</strong></td>
<td valign="top"><a href="#tripdtdtofilterinput">TripDtDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>any</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>