### PermissionAddDtoInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>roleId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>menuId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canCreate</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canView</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canAmend</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canApprove</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canConfirm</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canImport</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canExport</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canPrint</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#roleadddtoinput">RoleAddDtoInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>masterMenu</strong></td>
<td valign="top"><a href="#mastermenuadddtoinput">MasterMenuAddDtoInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### PermissionDtoFilterInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>and</strong></td>
<td valign="top">[<a href="#permissiondtofilterinput">PermissionDtoFilterInput</a>!]</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>or</strong></td>
<td valign="top">[<a href="#permissiondtofilterinput">PermissionDtoFilterInput</a>!]</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roleId</strong></td>
<td valign="top"><a href="#comparableguidoperationfilterinput">ComparableGuidOperationFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>menuId</strong></td>
<td valign="top"><a href="#comparableguidoperationfilterinput">ComparableGuidOperationFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canCreate</strong></td>
<td valign="top"><a href="#booleanoperationfilterinput">BooleanOperationFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canView</strong></td>
<td valign="top"><a href="#booleanoperationfilterinput">BooleanOperationFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canAmend</strong></td>
<td valign="top"><a href="#booleanoperationfilterinput">BooleanOperationFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canDelete</strong></td>
<td valign="top"><a href="#booleanoperationfilterinput">BooleanOperationFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canApprove</strong></td>
<td valign="top"><a href="#booleanoperationfilterinput">BooleanOperationFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canConfirm</strong></td>
<td valign="top"><a href="#booleanoperationfilterinput">BooleanOperationFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canImport</strong></td>
<td valign="top"><a href="#booleanoperationfilterinput">BooleanOperationFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canExport</strong></td>
<td valign="top"><a href="#booleanoperationfilterinput">BooleanOperationFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canPrint</strong></td>
<td valign="top"><a href="#booleanoperationfilterinput">BooleanOperationFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#roledtofilterinput">RoleDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>masterMenu</strong></td>
<td valign="top"><a href="#mastermenudtofilterinput">MasterMenuDtoFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#booleanoperationfilterinput">BooleanOperationFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#comparableguidoperationfilterinput">ComparableGuidOperationFilterInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### PermissionDtoSortInput

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>roleId</strong></td>
<td valign="top"><a href="#sortenumtype">SortEnumType</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>menuId</strong></td>
<td valign="top"><a href="#sortenumtype">SortEnumType</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canCreate</strong></td>
<td valign="top"><a href="#sortenumtype">SortEnumType</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canView</strong></td>
<td valign="top"><a href="#sortenumtype">SortEnumType</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canAmend</strong></td>
<td valign="top"><a href="#sortenumtype">SortEnumType</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canDelete</strong></td>
<td valign="top"><a href="#sortenumtype">SortEnumType</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canApprove</strong></td>
<td valign="top"><a href="#sortenumtype">SortEnumType</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canConfirm</strong></td>
<td valign="top"><a href="#sortenumtype">SortEnumType</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canImport</strong></td>
<td valign="top"><a href="#sortenumtype">SortEnumType</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canExport</strong></td>
<td valign="top"><a href="#sortenumtype">SortEnumType</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>canPrint</strong></td>
<td valign="top"><a href="#sortenumtype">SortEnumType</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#roledtosortinput">RoleDtoSortInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>masterMenu</strong></td>
<td valign="top"><a href="#mastermenudtosortinput">MasterMenuDtoSortInput</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#sortenumtype">SortEnumType</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#sortenumtype">SortEnumType</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>