### OrderAdditionalServiceDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>serviceId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>qty</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>orderId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cost</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sale</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCost</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalSale</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>free</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### OrderAddressDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>points</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addressDesc</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>containerLocationId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>orderId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>containerType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zoneId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>estimateDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>free</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>contactName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>countryId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>provinceId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cityId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>areaId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>postalCodeId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tel</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gpsLat</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gpsLon</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>country</strong></td>
<td valign="top"><a href="#mastercountrydto">MasterCountryDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>province</strong></td>
<td valign="top"><a href="#masterprovincedto">MasterProvinceDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>city</strong></td>
<td valign="top"><a href="#mastercitydto">MasterCityDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>area</strong></td>
<td valign="top"><a href="#masterareadto">MasterAreaDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>postalCode</strong></td>
<td valign="top"><a href="#masterpostalcodedto">MasterPostalCodeDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addressTypeList</strong></td>
<td valign="top">[<a href="#string">String</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### OrderAttachmentDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>orderId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>type</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>path</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### OrderCouponDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>orderId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>couponId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>amount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>discountType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>discountAmount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### OrderDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>orderNo</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shipmentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>requestPickupDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>statusCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>customerType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>couponCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vehicleId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>driverId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>routeStatusId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>customerId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>projectId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>invoiceNo</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>reciptNo</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>refNo1</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>refNo2</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>amount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>discountAmount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalAmount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalVatAmount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalAmountExVat</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>whtAmount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>netAmount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>grossAmount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>currency</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pendingTrip</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pendingInvoice</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>agentId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>orderAddress</strong></td>
<td valign="top">[<a href="#orderaddressdto">OrderAddressDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>orderAdditionalService</strong></td>
<td valign="top">[<a href="#orderadditionalservicedto">OrderAdditionalServiceDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>orderPrice</strong></td>
<td valign="top">[<a href="#orderpricedto">OrderPriceDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>orderRate</strong></td>
<td valign="top">[<a href="#orderratedto">OrderRateDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmPendingDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmPendingBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmOrderDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmOrderBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmOnProcessDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmOnProcessBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmCloseDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmCloseBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>podDescription</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>customer</strong></td>
<td valign="top"><a href="#customerdto">CustomerDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>project</strong></td>
<td valign="top"><a href="#customerprojectdto">CustomerProjectDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>coupon</strong></td>
<td valign="top"><a href="#ordercoupondto">OrderCouponDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vehicle</strong></td>
<td valign="top"><a href="#mastervehicledto">MasterVehicleDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>driver</strong></td>
<td valign="top"><a href="#driverdto">DriverDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>agent</strong></td>
<td valign="top"><a href="#agentdto">AgentDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>routeStatus</strong></td>
<td valign="top"><a href="#masterroutestatusdto">MasterRouteStatusDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### OrderDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#orderdto">OrderDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### OrderPriceDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>orderId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>transportationFee</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>agentId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>refNo1</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>refNo2</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>priceType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vatInclude</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>qty</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>amount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vatRate</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>whtRate</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>discountType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>discount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>discountAmount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vatAmount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>amountExVat</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>whtAmount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalAmount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>netAmount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>grossAmount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>apNo</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>invoiceNo</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pendingAp</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>agent</strong></td>
<td valign="top"><a href="#agentdto">AgentDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>order</strong></td>
<td valign="top"><a href="#orderdto">OrderDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### OrderPriceDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#orderpricedto">OrderPriceDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### OrderProveOfDeliveryDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>orderId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachment</strong></td>
<td valign="top">[<a href="#orderattachmentdto">OrderAttachmentDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>podDescription</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### OrderRateDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>orderId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cost</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sale</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>seqNo</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### OrderTrackingDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>trackingDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>orderId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>statusId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>reasonId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>remarks</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>order</strong></td>
<td valign="top"><a href="#orderdto">OrderDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top"><a href="#masterstatusdto">MasterStatusDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>reason</strong></td>
<td valign="top"><a href="#masterreasonstatusdto">MasterReasonStatusDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### OrderTrackingDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#ordertrackingdto">OrderTrackingDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>