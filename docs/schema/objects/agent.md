### AgentAddressDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>agentId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addressType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>contactName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>countryId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>provinceId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cityId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>areaId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>postalCodeId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tel</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gpsLat</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gpsLon</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>country</strong></td>
<td valign="top"><a href="#mastercountrydto">MasterCountryDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>province</strong></td>
<td valign="top"><a href="#masterprovincedto">MasterProvinceDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>city</strong></td>
<td valign="top"><a href="#mastercitydto">MasterCityDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>area</strong></td>
<td valign="top"><a href="#masterareadto">MasterAreaDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>postalCode</strong></td>
<td valign="top"><a href="#masterpostalcodedto">MasterPostalCodeDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addressTypeList</strong></td>
<td valign="top">[<a href="#string">String</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
</tbody>
</table>

### AgentAddressDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#agentaddressdto">AgentAddressDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
</tbody>
</table>

### AgentDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>taxId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top">[<a href="#agentaddressdto">AgentAddressDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vehicle</strong></td>
<td valign="top">[<a href="#agentvehicledto">AgentVehicleDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
</tbody>
</table>

### AgentDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#agentdto">AgentDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
</tbody>
</table>

### AgentVehicleDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>caution</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>serviceCondition</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>agentId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vehicleId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>license</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>driverId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasDim</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>length</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>width</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>height</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxPayLoad</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxLiftingWeight</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cbm</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>onlyOneArticle</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vehicle</strong></td>
<td valign="top"><a href="#mastervehicledto">MasterVehicleDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>driver</strong></td>
<td valign="top"><a href="#driverdto">DriverDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
</tbody>
</table>

### AgentVehicleDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#agentvehicledto">AgentVehicleDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
</tr>
</tbody>
</table>