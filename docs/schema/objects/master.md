### MasterAreaDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nameTh</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>countryId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>provinceId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cityId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>country</strong></td>
<td valign="top"><a href="#mastercountrydto">MasterCountryDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>province</strong></td>
<td valign="top"><a href="#masterprovincedto">MasterProvinceDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>city</strong></td>
<td valign="top"><a href="#mastercitydto">MasterCityDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterAreaDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#masterareadto">MasterAreaDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterCityDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nameTh</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>countryId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>provinceId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>country</strong></td>
<td valign="top"><a href="#mastercountrydto">MasterCountryDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>province</strong></td>
<td valign="top"><a href="#masterprovincedto">MasterProvinceDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterCityDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#mastercitydto">MasterCityDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterContainerLocationDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>containerType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addressType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>contactName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>countryId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>provinceId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cityId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>areaId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>postalCodeId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>tel</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gpsLat</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gpsLon</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>country</strong></td>
<td valign="top"><a href="#mastercountrydto">MasterCountryDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>province</strong></td>
<td valign="top"><a href="#masterprovincedto">MasterProvinceDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>city</strong></td>
<td valign="top"><a href="#mastercitydto">MasterCityDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>area</strong></td>
<td valign="top"><a href="#masterareadto">MasterAreaDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>postalCode</strong></td>
<td valign="top"><a href="#masterpostalcodedto">MasterPostalCodeDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addressTypeList</strong></td>
<td valign="top">[<a href="#string">String</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterCountryDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nameTh</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterCountryDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#mastercountrydto">MasterCountryDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterDiscountDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>discountType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>amount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>validUntil</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>imgUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterMenuDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>seq</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>i18n</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>link</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>icon</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isGroup</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hideInBreadcrumb</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterMenuDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#mastermenudto">MasterMenuDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterPostalCodeDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>countryId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>provinceId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cityId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>areaId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>country</strong></td>
<td valign="top"><a href="#mastercountrydto">MasterCountryDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>province</strong></td>
<td valign="top"><a href="#masterprovincedto">MasterProvinceDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>city</strong></td>
<td valign="top"><a href="#mastercitydto">MasterCityDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>area</strong></td>
<td valign="top"><a href="#masterareadto">MasterAreaDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterPostalCodeDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#masterpostalcodedto">MasterPostalCodeDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterProvinceDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nameTh</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>countryId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>country</strong></td>
<td valign="top"><a href="#mastercountrydto">MasterCountryDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterProvinceDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#masterprovincedto">MasterProvinceDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterRateDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>agentId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vehicleId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zoneIdFrom</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zoneIdTo</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cost</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sale</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>agent</strong></td>
<td valign="top"><a href="#agentdto">AgentDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vehicle</strong></td>
<td valign="top"><a href="#mastervehicledto">MasterVehicleDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zoneFrom</strong></td>
<td valign="top"><a href="#masterzonedto">MasterZoneDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zoneTo</strong></td>
<td valign="top"><a href="#masterzonedto">MasterZoneDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterRateDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#masterratedto">MasterRateDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterReasonStatusDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>statusId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>internalReason</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>externalReason</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>masterStatus</strong></td>
<td valign="top"><a href="#masterstatusdto">MasterStatusDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterReasonStatusDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#masterreasonstatusdto">MasterReasonStatusDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterRouteStatusDetailDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>routeStatusId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>statusId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>routeStatus</strong></td>
<td valign="top"><a href="#masterroutestatusdto">MasterRouteStatusDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>masterStatus</strong></td>
<td valign="top"><a href="#masterstatusdto">MasterStatusDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterRouteStatusDetailDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#masterroutestatusdetaildto">MasterRouteStatusDetailDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterRouteStatusDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>customerId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>projectId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>customer</strong></td>
<td valign="top"><a href="#customerdto">CustomerDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>customerProject</strong></td>
<td valign="top"><a href="#customerprojectdto">CustomerProjectDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterRouteStatusDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#masterroutestatusdto">MasterRouteStatusDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterServiceDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>free</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cost</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sale</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>currency</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>requireAdditional</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>children</strong></td>
<td valign="top">[<a href="#masterservicedto">MasterServiceDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterServiceDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#masterservicedto">MasterServiceDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterStatusDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>photoRequired</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isRequired</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isInternal</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isExternal</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterStatusDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#masterstatusdto">MasterStatusDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterVehicleDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>freeTime</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasDim</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>length</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>width</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>height</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxPayLoad</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>maxLiftingWeight</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vehicleTypeId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cbm</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cost</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sale</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>currency</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vehicleType</strong></td>
<td valign="top"><a href="#mastervehicletypedto">MasterVehicleTypeDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>imgUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isHeader</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterVehicleDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#mastervehicledto">MasterVehicleDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterVehicleServiceDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>parentCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>free</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cost</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sale</strong></td>
<td valign="top"><a href="#decimal">Decimal</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>masterVehicleId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>serviceId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>additionalService</strong></td>
<td valign="top"><a href="#masterservicedto">MasterServiceDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>masterVehicle</strong></td>
<td valign="top"><a href="#mastervehicledto">MasterVehicleDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterVehicleServiceDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#mastervehicleservicedto">MasterVehicleServiceDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterVehicleTypeDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterZoneDetailDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>zoneId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>countryId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>provinceId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>cityId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>areaId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>postalCodeId</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zone</strong></td>
<td valign="top"><a href="#masterzonedto">MasterZoneDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>country</strong></td>
<td valign="top"><a href="#mastercountrydto">MasterCountryDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>province</strong></td>
<td valign="top"><a href="#masterprovincedto">MasterProvinceDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>city</strong></td>
<td valign="top"><a href="#mastercitydto">MasterCityDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>area</strong></td>
<td valign="top"><a href="#masterareadto">MasterAreaDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>postalCode</strong></td>
<td valign="top"><a href="#masterpostalcodedto">MasterPostalCodeDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterZoneDetailDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#masterzonedetaildto">MasterZoneDetailDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterZoneDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>origin</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>destination</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dropPointType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### MasterZoneDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#masterzonedto">MasterZoneDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>