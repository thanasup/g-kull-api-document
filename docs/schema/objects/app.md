### AppDataMenuDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#usermenudto">UserMenuDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>app</strong></td>
<td valign="top"><a href="#appinfomenudto">AppInfoMenuDto</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>menu</strong></td>
<td valign="top">[<a href="#permissionmenudto">PermissionMenuDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### AppInfoMenuDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
</tr>
</tbody>
</table>