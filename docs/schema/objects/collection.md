### CollectionSegmentInfo

Information about the offset pagination.

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>hasNextPage</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Indicates whether more items exist following the set defined by the clients arguments.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasPreviousPage</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Indicates whether more items exist prior the set defined by the clients arguments.

</td>
</tr>
</tbody>
</table>