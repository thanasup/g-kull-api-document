### RoleDto

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isActive</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isDelete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedBy</strong></td>
<td valign="top"><a href="#uuid">Uuid</a></td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#uuid">Uuid</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>

### RoleDtoCollectionSegment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Required</th>
<th align="left">MaxLength</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#roledto">RoleDto</a>]</td>
<td valign="top">-</td>
<td valign="top"></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pageInfo</strong></td>
<td valign="top"><a href="#collectionsegmentinfo">CollectionSegmentInfo</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td>

Information to aid in pagination.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>totalCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td valign="top">True</td>
<td valign="top"></td>
<td></td>
</tr>
</tbody>
</table>