[Back to readme](../README.md)

> **SCHEMA**

- [MasterVehicleDtoSortInput](../schema/inputs/master.md#mastervehicledtosortinput)
- [MasterVehicleDtoFilterInput](../schema/inputs/master.md#mastervehicledtofilterinput)
- [MasterVehicleDto](../schema/objects/master.md#mastervehicledto)

# VehiclePagedList

> **QUERY**

```
query VehiclePagedList($skip: Int, $take: Int, $order: [MasterVehicleDtoSortInput!], $where: MasterVehicleDtoFilterInput) {
  vehiclePagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...MasterVehicleDto
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterVehicleDtoSortInput!
  ] ,
  "where": MasterVehicleDtoFilterInput,
}
```

# VehicleList

> **QUERY**

```
query VehicleList($order: [MasterVehicleDtoSortInput!], $where: MasterVehicleDtoFilterInput) {
  vehicleList(order: $order, where: $where) {
    ...MasterVehicleDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterVehicleDtoSortInput!
  ] ,
  "where": MasterVehicleDtoFilterInput,
}
```

# Vehicle

> **QUERY**

```
query Vehicle($id: Uuid!) {
  vehicle(id: $id) {
    ...MasterVehicleDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```

# vehicleGroupType

> **QUERY**

```
query vehicleGroupType {
  vehicleGroupType {
    ...MasterVehicleDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{}
```
