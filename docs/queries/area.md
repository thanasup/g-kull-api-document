[Back to readme](../README.md)

> **SCHEMA**

- [MasterAreaDtoSortInput](../schema/inputs/master.md#masterareadtosortinput)
- [MasterAreaDtoFilterInput](../schema/inputs/master.md#masterareadtofilterinput)
- [country](../schema/inputs/master.md#mastercountryadddtoinput)
- [province](../schema/inputs/master.md#masterprovinceadddtoinput)
- [city](../schema/inputs/master.md#mastercityadddtoinput)

# AreaPagedList

> **QUERY**

```
query AreaPagedList($skip: Int, $take: Int, $order: [MasterAreaDtoSortInput!], $where: MasterAreaDtoFilterInput) {
  areaPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      code
      name
      nameTh
      country {
        name
        nameTh
      }
      province {
        name
        nameTh
      }
      city {
        name
        nameTh
      }
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
      id
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterAreaDtoSortInput!
  ] ,
  "where": MasterAreaDtoFilterInput,
}
```

# AreaList

> **QUERY**

```
query AreaList($order: [MasterAreaDtoSortInput!], $where: MasterAreaDtoFilterInput) {
  areaList(order: $order, where: $where) {
    code
    name
    nameTh
    country {
      name
      nameTh
    }
    province {
      name
      nameTh
    }
    city {
      name
      nameTh
    }
    id
  }
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterAreaDtoSortInput!
  ] ,
  "where": MasterAreaDtoFilterInput,
}
```

# Area

> **QUERY**

```
query Area($id: Uuid!) {
  area(id: $id) {
    code
    name
    nameTh
    countryId
    country {
      name
      nameTh
    }
    provinceId
    province {
      name
      nameTh
    }
    cityId
    city {
      name
      nameTh
    }
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
