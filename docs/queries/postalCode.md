[Back to readme](../README.md)

> **SCHEMA**

- [MasterPostalCodeDtoSortInput](../schema/inputs/master.md#masterpostalcodedtosortinput)
- [MasterPostalCodeDtoFilterInput](../schema/inputs/master.md#masterpostalcodedtofilterinput)
- [country](../schema/inputs/master.md#mastercountryadddtoinput)
- [province](../schema/inputs/master.md#masterprovinceadddtoinput)
- [city](../schema/inputs/master.md#mastercityadddtoinput)
- [area](../schema/inputs/master.md#masterareaadddtoinput)

# PostalCodePagedList

> **QUERY**

```
query PostalCodePagedList($skip: Int, $take: Int, $order: [MasterPostalCodeDtoSortInput!], $where: MasterPostalCodeDtoFilterInput) {
  postalCodePagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      code
      country {
        name
        nameTh
      }
      province {
        name
        nameTh
      }
      city {
        name
        nameTh
      }
      area {
        name
        nameTh
      }
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
      id
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterPostalCodeDtoSortInput!
  ] ,
  "where": MasterPostalCodeDtoFilterInput,
}
```

# PostalCodeList

> **QUERY**

```
query PostalCodeList($order: [MasterPostalCodeDtoSortInput!], $where: MasterPostalCodeDtoFilterInput) {
  postalCodeList(order: $order, where: $where) {
    code
    country {
      name
      nameTh
    }
    province {
      name
      nameTh
    }
    city {
      name
      nameTh
    }
    area {
      name
      nameTh
    }
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterPostalCodeDtoSortInput!
  ] ,
  "where": MasterPostalCodeDtoFilterInput,
}
```

# PostalCode

> **QUERY**

```
query PostalCode($id: Uuid!) {
  postalCode(id: $id) {
    code
    countryId
    country {
      name
      nameTh
    }
    provinceId
    province {
      name
      nameTh
    }
    cityId
    city {
      name
      nameTh
    }
    areaId
    area {
      name
      nameTh
    }
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
