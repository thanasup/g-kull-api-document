[Back to readme](../README.md)

> **SCHEMA**

- [MasterZoneDtoSortInput](../schema/inputs/master.md#masterzonedtosortinput)
- [MasterZoneDtoFilterInput](../schema/inputs/master.md#masterzonedtofilterinput)
- [MasterZoneDto](../schema/objects/master.md#masterzonedto)

# ZonePagedList

> **QUERY**

```
query zonePagedList($skip: Int, $take: Int, $order: [MasterZoneDtoSortInput!], $where: MasterZoneDtoFilterInput) {
  zonePagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      origin
      destination
      code
      name
      description
      id
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterZoneDtoSortInput!
  ] ,
  "where": MasterZoneDtoFilterInput,
}
```

# ZoneList

> **QUERY**

```
query zoneList($order: [MasterZoneDtoSortInput!], $where: MasterZoneDtoFilterInput) {
  zoneList(order: $order, where: $where) {
    origin
    destination
    code
    name
    description
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterZoneDtoSortInput!
  ] ,
  "where": MasterZoneDtoFilterInput,
}
```

# Zone

> **QUERY**

```
query zone($id: Uuid!) {
  zone(id: $id) {
    origin
    destination
    code
    name
    description
    id
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
