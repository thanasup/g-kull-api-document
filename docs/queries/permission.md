[Back to readme](../README.md)

> **SCHEMA**

- [PermissionDtoSortInput](../schema/inputs/permission.md#permissiondtosortinput)
- [PermissionDtoFilterInput](../schema/inputs/permission.md#permissiondtofilterinput)
- [PermissionDto](../schema/objects/permission.md#permissiondto)

# PermissionPagedList

> **QUERY**

```
query permissionPagedList($skip: Int, $take: Int, $roleId: Uuid!, $parentCode: String, $order: [PermissionDtoSortInput!], $where: PermissionDtoFilterInput) {
  permissionPagedList(
    skip: $skip
    take: $take
    roleId: $roleId
    parentCode: $parentCode
    order: $order
    where: $where
  ) {
    items {
      ...PermissionDto
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    PermissionDtoSortInput!
  ] ,
  "where": PermissionDtoFilterInput,
}
```

# PermissionList

> **QUERY**

```
query permissionList($order: [PermissionDtoSortInput!], $roleId: Uuid!, $parentCode: String, $where: PermissionDtoFilterInput) {
  permissionList(
    order: $order
    roleId: $roleId
    parentCode: $parentCode
    where: $where
  ) {
    ...PermissionDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "roleId": "",
  "parentCode": "",
  "order": [
    PermissionDtoSortInput!
  ] ,
  "where": PermissionDtoFilterInput,
}
```

# Permission

> **QUERY**

```
query permission($roleId: Uuid!) {
  permission(roleId: $roleId) {
    ...PermissionDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "roleId": ""
}
```
