[Back to readme](../README.md)

> **SCHEMA**

- [CustomerAddressDtoSortInput](../schema/inputs/customer.md#customeraddressdtosortinput)
- [CustomerAddressDtoFilterInput](../schema/inputs/customer.md#customeraddressdtofilterinput)
- [CustomerAddressDto](../schema/objects/customer.md#customeraddressdto)

# CustomerAddressPagedList

> **QUERY**

```
query customerAddressPagedList($skip: Int, $take: Int, $order: [CustomerAddressDtoSortInput!], $where: CustomerAddressDtoFilterInput) {
  customerAddressPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...CustomerAddressDto
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    CustomerAddressDtoSortInput!
  ] ,
  "where": CustomerAddressDtoFilterInput,
}
```

# CustomerAddressList

> **QUERY**

```
query customerAddressList($where: CustomerAddressDtoFilterInput) {
  customerAddressList(where: $where) {
    ...CustomerAddressDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "where": CustomerAddressDtoFilterInput,
}
```

# CustomerAddress

> **QUERY**

```
query customerAddress($addressId: Uuid!) {
  customerAddress(addressId: $addressId) {
    ...CustomerAddressDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": ""
}
```
