[Back to readme](../README.md)

> **SCHEMA**

- [MasterRouteStatusDetailDtoSortInput](../schema/inputs/master.md#masterroutestatusdetaildtosortinput)
- [MasterRouteStatusDetailDtoFilterInput](../schema/inputs/master.md#masterroutestatusdetaildtofilterinput)
- [MasterRouteStatusDetailDto](../schema/objects/master.md#masterroutestatusdetaildto)
- [routeStatus](../schema/inputs/master.md#masterroutestatusadddtoinput)
- [masterStatus](../schema/inputs/master.md#masterstatusadddtoinput)

# RouteStatusDetailPagedList

> **QUERY**

```
query RouteStatusDetailPagedList($skip: Int, $take: Int, $order: [MasterRouteStatusDetailDtoSortInput!], $where: MasterRouteStatusDetailDtoFilterInput) {
  routeStatusDetailPagedList(
    skip: $skip
    take: $take
    order: $order
    where: $where
  ) {
    items {
      ...MasterRouteStatusDetailDto
      routeStatus {
        code
        name: code
      }
      masterStatus {
        code
        name
      }
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterRouteStatusDetailDtoSortInput!
  ] ,
  "where": MasterRouteStatusDetailDtoFilterInput,
}
```

# RouteStatusList

> **QUERY**

```
query RouteStatusDetailList($order: [MasterRouteStatusDetailDtoSortInput!], $where: MasterRouteStatusDetailDtoFilterInput) {
  routeStatusDetailList(order: $order, where: $where) {
    ...MasterRouteStatusDetailDto
    routeStatus {
      code
      name: code
    }
    masterStatus {
      code
      name
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterRouteStatusDetailDtoSortInput!
  ] ,
  "where": MasterRouteStatusDetailDtoFilterInput,
}
```

# RouteStatus

> **QUERY**

```
query RouteStatusDetail($id: Uuid!) {
  routeStatusDetail(id: $id) {
    ...MasterRouteStatusDetailDto
    routeStatus {
      code
      name: code
    }
    masterStatus {
      code
      name
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
