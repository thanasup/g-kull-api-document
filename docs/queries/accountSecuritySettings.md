[Back to readme](../README.md)

# AccountSecuritySettings

> **QUERY**

```
query AccountSecuritySettings($id: Uuid!) {
  accountSecuritySettings(id: $id) {
    username
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
