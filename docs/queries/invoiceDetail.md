[Back to readme](../README.md)

> **SCHEMA**

- [InvoiceDtDtoSortInput](../schema/inputs/invoice.md#invoicedtdtosortinput)
- [InvoiceDtDtoFilterInput](../schema/inputs/invoice.md#invoicedtdtofilterinput)
- [InvoiceDtDto](../schema/objects/invoice.md#invoicedtdto)

# InvoiceDetail

> **QUERY**

```
query invoiceDetail($invoiceId: Uuid!) {
  invoiceDetail(invoiceId: $invoiceId) {
    ...InvoiceDtDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "invoiceId": ""
}
```

# InvoiceDetailPagedList

> **QUERY**

```
query invoiceDetailPagedList($skip: Int, $take: Int, $order: [InvoiceDtDtoSortInput!], $where: InvoiceDtDtoFilterInput) {
  invoiceDetailPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...InvoiceDtDto
      createdDate
      createdBy
      updatedDate
      updatedBy
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    InvoiceDtDtoSortInput!
  ] ,
  "where": InvoiceDtDtoFilterInput,
}
```
