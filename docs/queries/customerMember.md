[Back to readme](../README.md)

> **SCHEMA**

- [CustomerMemberDtoSortInput](../schema/inputs/customer.md#customermemberdtosortinput)
- [CustomerMemberDtoFilterInput](../schema/inputs/customer.md#customermemberdtofilterinput)
- [CustomerMemberDto](../schema/objects/customer.md#customermemberdto)

# CustomerMemberPagedList

> **QUERY**

```
query customerMemberPagedList($skip: Int, $take: Int, $order: [CustomerMemberDtoSortInput!], $where: CustomerMemberDtoFilterInput) {
  customerMemberPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...CustomerMemberDto
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    CustomerMemberDtoSortInput!
  ] ,
  "where": CustomerMemberDtoFilterInput,
}
```

# CustomerMemberList

> **QUERY**

```
query customerMemberList($where: CustomerMemberDtoFilterInput) {
  customerMemberList(where: $where) {
    ...CustomerMemberDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "where": CustomerMemberDtoFilterInput,
}
```

# CustomerMember

> **QUERY**

```
query customerMember($memberId: Uuid!) {
  customerMember(memberId: $memberId) {
    ...CustomerMemberDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "memberId": ""
}
```
