[Back to readme](../README.md)

> **SCHEMA**

- [MasterServiceDtoSortInput](../schema/inputs/master.md#masterservicedtosortinput)
- [MasterServiceDtoFilterInput](../schema/inputs/master.md#masterservicedtofilterinput)
- [MasterServiceDto](../schema/objects/master.md#masterservicedto)

# ServicePagedList

> **QUERY**

```
query servicePagedList($skip: Int, $take: Int, $order: [MasterServiceDtoSortInput!], $where: MasterServiceDtoFilterInput) {
  servicePagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...MasterServiceDto
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterServiceDtoSortInput!
  ] ,
  "where": MasterServiceDtoFilterInput,
}
```

# ServiceList

> **QUERY**

```
query serviceList($order: [MasterServiceDtoSortInput!], $where: MasterServiceDtoFilterInput) {
  serviceList(order: $order, where: $where) {
    ...MasterServiceDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterServiceDtoSortInput!
  ] ,
  "where": MasterServiceDtoFilterInput,
}
```

# Service

> **QUERY**

```
query service($id: Uuid!) {
  service(id: $id) {
    ...MasterServiceDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```

# ServiceTree

> **QUERY**

```
query serviceTree {
  serviceTree {
    ...MasterServiceDto
    children {
      ...MasterServiceDto
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{}
```
