[Back to readme](../README.md)

> **SCHEMA**

- [MasterStatusDtoSortInput](../schema/inputs/master.md#masterstatusdtosortinput)
- [MasterStatusDtoFilterInput](../schema/inputs/master.md#masterstatusdtofilterinput)
- [MasterStatusDto](../schema/objects/master.md#masterstatusdto)

# StatusPagedList

> **QUERY**

```
query StatusPagedList($skip: Int, $take: Int, $order: [MasterStatusDtoSortInput!], $where: MasterStatusDtoFilterInput) {
  statusPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...MasterStatusDto
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterStatusDtoSortInput!
  ] ,
  "where": MasterStatusDtoFilterInput,
}
```

# StatusList

> **QUERY**

```
query StatusList($order: [MasterStatusDtoSortInput!], $where: MasterStatusDtoFilterInput) {
  statusList(order: $order, where: $where) {
    ...MasterStatusDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterStatusDtoSortInput!
  ] ,
  "where": MasterStatusDtoFilterInput,
}
```

# Status

> **QUERY**

```
query Status($id: Uuid!) {
  status(id: $id) {
    ...MasterStatusDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
