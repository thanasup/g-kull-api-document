[Back to readme](../README.md)

> **SCHEMA**

- [MasterZoneDetailDtoSortInput](../schema/inputs/master.md#masterzonedetaildtosortinput)
- [MasterZoneDetailDtoFilterInput](../schema/inputs/master.md#masterzonedetaildtofilterinput)
- [MasterZoneDetailDto](../schema/objects/master.md#masterzonedetaildto)
- [zone](../schema/inputs/master.md#masterzoneadddtoinput)
- [country](../schema/inputs/master.md#mastercountryadddtoinput)
- [province](../schema/inputs/master.md#masterprovinceadddtoinput)
- [city](../schema/inputs/master.md#mastercityadddtoinput)
- [area](../schema/inputs/master.md#masterareaadddtoinput)
- [postalCode](../schema/inputs/master.md#masterpostalcodeadddtoinput)

# ZoneDetailPagedList

> **QUERY**

```
query zoneDetailPagedList($skip: Int, $take: Int, $order: [MasterZoneDetailDtoSortInput!], $where: MasterZoneDetailDtoFilterInput) {
  zoneDetailPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      zoneId
      countryId
      provinceId
      cityId
      areaId
      postalCodeId
      zone {
        origin
        destination
        code
        name
        description
      }
      country {
        code
        name
        nameTh
      }
      province {
        code
        name
        nameTh
      }
      city {
        code
        name
        nameTh
      }
      area {
        code
        name
        nameTh
      }
      postalCode {
        code
      }
      id
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterZoneDetailDtoSortInput!
  ] ,
  "where": MasterZoneDetailDtoFilterInput,
}
```

# ZoneDetailList

> **QUERY**

```
query zoneDetailList($order: [MasterZoneDetailDtoSortInput!], $where: MasterZoneDetailDtoFilterInput) {
  zoneDetailList(order: $order, where: $where) {
    zoneId
    countryId
    provinceId
    cityId
    areaId
    postalCodeId
    zone {
      origin
      destination
      code
      name
      description
    }
    country {
      code
      name
      nameTh
    }
    province {
      code
      name
      nameTh
    }
    city {
      code
      name
      nameTh
    }
    area {
      code
      name
      nameTh
    }
    postalCode {
      code
    }
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterZoneDetailDtoSortInput!
  ] ,
  "where": MasterZoneDetailDtoFilterInput,
}
```

# ZoneDetail

> **QUERY**

```
query zoneDetail($id: Uuid!) {
  zoneDetail(id: $id) {
    zoneId
    countryId
    provinceId
    cityId
    areaId
    postalCodeId
    zone {
      origin
      destination
      code
      name
      description
    }
    country {
      code
      name
      nameTh
    }
    province {
      code
      name
      nameTh
    }
    city {
      code
      name
      nameTh
    }
    area {
      code
      name
      nameTh
    }
    postalCode {
      code
    }
    id
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
