[Back to readme](../README.md)

> **SCHEMA**

- [MasterRouteStatusDtoSortInput](../schema/inputs/master.md#masterroutestatusdtosortinput)
- [MasterRouteStatusDtoFilterInput](../schema/inputs/master.md#masterroutestatusdtofilterinput)
- [MasterRouteStatusDto](../schema/objects/master.md#masterroutestatusdto)
- [customer](../schema/inputs/customer.md#customerupdatedtoinput)
- [customerProject](../schema/inputs/customer.md#customerprojectadddtoinput)

# RouteStatusPagedList

> **QUERY**

```
query RouteStatusPagedList($skip: Int, $take: Int, $order: [MasterRouteStatusDtoSortInput!], $where: MasterRouteStatusDtoFilterInput) {
  routeStatusPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...MasterRouteStatusDto
      customer {
        code
        name: companyName
      }
      customerProject {
        code
        name
      }
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterRouteStatusDtoSortInput!
  ] ,
  "where": MasterRouteStatusDtoFilterInput,
}
```

# RouteStatusList

> **QUERY**

```
query RouteStatusList($order: [MasterRouteStatusDtoSortInput!], $where: MasterRouteStatusDtoFilterInput) {
  routeStatusList(order: $order, where: $where) {
    ...MasterRouteStatusDto
    customer {
      code
      name: companyName
    }
    customerProject {
      code
      name
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterRouteStatusDtoSortInput!
  ] ,
  "where": MasterRouteStatusDtoFilterInput,
}
```

# RouteStatus

> **QUERY**

```
query RouteStatus($id: Uuid!) {
  routeStatus(id: $id) {
    ...MasterRouteStatusDto
    customer {
      code
      name: companyName
    }
    customerProject {
      code
      name
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
