[Back to readme](../README.md)

# AccountUpdateBaseSettings

> **QUERY**

```
query AccountBaseSettings($id: Uuid!) {
  accountBaseSettings(id: $id) {
    fullName
    role {
      code
      name
      description
      id
    }
    email
    isVerified
    verified
    code
    title
    firstName
    lastName
    phone
    isActive
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
