[Back to readme](../README.md)

> **SCHEMA**

- [MasterMenuDtoSortInput](../schema/inputs/master.md#mastermenudtosortinput)
- [MasterMenuDtoFilterInput](../schema/inputs/master.md#mastermenudtofilterinput)
- [MasterMenuDto](../schema/objects/master.md#mastermenudto)

# MenuPagedList

> **QUERY**

```
query menuPagedList($skip: Int, $take: Int, $order: [MasterMenuDtoSortInput!], $where: MasterMenuDtoFilterInput) {
  menuPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...MasterMenuDto
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterMenuDtoSortInput!
  ] ,
  "where": MasterMenuDtoFilterInput,
}
```

# MenuList

> **QUERY**

```
query menuList($order: [MasterMenuDtoSortInput!], $where: MasterMenuDtoFilterInput) {
  menuList(order: $order, where: $where) {
    ...MasterMenuDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterMenuDtoSortInput!
  ] ,
  "where": MasterMenuDtoFilterInput,
}
```

# Menu

> **QUERY**

```
query menu($id: Uuid!) {
  menu(id: $id) {
    ...MasterMenuDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
