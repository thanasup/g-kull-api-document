[Back to readme](../README.md)

> **SCHEMA**

- [MasterVehicleTypeDtoSortInput](../schema/inputs/master.md#mastervehicletypedtosortinput)
- [MasterVehicleTypeDtoFilterInput](../schema/inputs/master.md#mastervehicletypedtofilterinput)
- [MasterVehicleTypeDto](../schema/objects/master.md#mastervehicletypedto)

# VehicleTypeList

> **QUERY**

```
query vehicleTypeList($order: [MasterVehicleTypeDtoSortInput!], $where: MasterVehicleTypeDtoFilterInput) {
  vehicleTypeList(order: $order, where: $where) {
    ...MasterVehicleTypeDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterVehicleTypeDtoSortInput!
  ] ,
  "where": MasterVehicleTypeDtoFilterInput,
}
```
