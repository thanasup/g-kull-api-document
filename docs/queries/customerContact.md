[Back to readme](../README.md)

> **SCHEMA**

- [CustomerContactDtoSortInput](../schema/inputs/customer.md#customercontactdtosortinput)
- [CustomerContactDtoFilterInput](../schema/inputs/customer.md#customercontactdtofilterinput)
- [CustomerContactDto](../schema/objects/customer.md#customercontactdto)

# CustomerContactPagedList

> **QUERY**

```
query customerContactPagedList($skip: Int, $take: Int, $order: [CustomerContactDtoSortInput!], $where: CustomerContactDtoFilterInput) {
  customerContactPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...CustomerContactDto
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    CustomerContactDtoSortInput!
  ] ,
  "where": CustomerContactDtoFilterInput,
}
```

# CustomerContactList

> **QUERY**

```
query customerContactList($where: CustomerContactDtoFilterInput) {
  customerContactList(where: $where) {
    ...CustomerContactDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "where": CustomerContactDtoFilterInput,
}
```

# CustomerContact

> **QUERY**

```
query customerContact($contactId: Uuid!) {
  customerContact(contactId: $contactId) {
    ...CustomerContactDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "contactId": ""
}
```
