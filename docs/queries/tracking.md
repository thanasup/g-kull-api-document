[Back to readme](../README.md)

> **SCHEMA**

- [OrderTrackingDtoSortInput](../schema/inputs/order.md#ordertrackingdtosortinput)
- [OrderTrackingDtoFilterInput](../schema/inputs/order.md#ordertrackingdtofilterinput)
- [OrderTrackingDto](../schema/objects/order.md#ordertrackingdto)

# TrackingPagedList

> **QUERY**

```
query TrackingPagedList($skip: Int, $take: Int, $order: [OrderTrackingDtoSortInput!], $where: OrderTrackingDtoFilterInput) {
  trackingPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...OrderTrackingDto
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "ordertracking": [
    OrderTrackingDtoSortInput!
  ] ,
  "where": OrderTrackingDtoFilterInput,
}
```

# TrackingList

> **QUERY**

```
query TrackingList($order: [OrderTrackingDtoSortInput!], $where: OrderTrackingDtoFilterInput) {
  trackingList(order: $order, where: $where) {
    ...OrderTrackingDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "ordertracking": [
    OrderTrackingDtoSortInput!
  ] ,
  "where": OrderTrackingDtoFilterInput,
}
```

# Tracking

> **QUERY**

```
query Tracking($id: Uuid!) {
  tracking(id: $id) {
    ...OrderTrackingDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
