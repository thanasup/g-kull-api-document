[Back to readme](../README.md)

> **SCHEMA**

- [MasterProvinceDtoSortInput](../schema/inputs/master.md#masterprovincedtosortinput)
- [MasterProvinceDtoFilterInput](../schema/inputs/master.md#masterprovincedtofilterinput)
- [country](../schema/inputs/master.md#mastercountryadddtoinput)

# ProvincePagedList

> **QUERY**

```
query ProvincePagedList($skip: Int, $take: Int, $order: [MasterProvinceDtoSortInput!], $where: MasterProvinceDtoFilterInput) {
  provincePagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      code
      name
      nameTh
      country {
        name
        nameTh
      }
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
      id
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterProvinceDtoSortInput!
  ] ,
  "where": MasterProvinceDtoFilterInput,
}
```

# ProvinceList

> **QUERY**

```
query ProvinceList($order: [MasterProvinceDtoSortInput!], $where: MasterProvinceDtoFilterInput) {
  provinceList(order: $order, where: $where) {
    code
    name
    nameTh
    country {
      name
      nameTh
    }
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterProvinceDtoSortInput!
  ] ,
  "where": MasterProvinceDtoFilterInput,
}
```

# Province

> **QUERY**

```
query Province($id: Uuid!) {
  province(id: $id) {
    code
    name
    nameTh
    country {
      name
      nameTh
    }
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
