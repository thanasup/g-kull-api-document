[Back to readme](../README.md)

> **SCHEMA**

- [MasterDiscountDtoSortInput](../schema/inputs/master.md#masterdiscountdtosortinput)
- [MasterDiscountDtoFilterInput](../schema/inputs/master.md#masterdiscountdtofilterinput)
- [MasterDiscountDto](../schema/objects/master.md#masterdiscountdto)

# MasterDiscountList

> **QUERY**

```
query masterDiscountList($where: MasterDiscountDtoFilterInput, $order: [MasterDiscountDtoSortInput!]) {
  masterDiscountList(where: $where, order: $order) {
    ...MasterDiscountDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterDiscountDtoSortInput!
  ] ,
  "where": MasterDiscountDtoFilterInput,
}
```
