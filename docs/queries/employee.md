[Back to readme](../README.md)

> **SCHEMA**

- [EmployeeDtoSortInput](../schema/inputs/employee.md#employeedtosortinput)
- [EmployeeDtoFilterInput](../schema/inputs/employee.md#employeedtofilterinput)
- [EmployeeDto](../schema/objects/employee.md#employeedto)

# EmployeePagedList

> **QUERY**

```
query EmployeePagedList($skip: Int, $take: Int, $order: [EmployeeDtoSortInput!], $where: EmployeeDtoFilterInput) {
  employeePagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...EmployeeDto
      createdDate
      createdBy
      updatedDate
      updatedBy
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    EmployeeDtoSortInput!
  ] ,
  "where": EmployeeDtoFilterInput,
}
```

# EmployeeList

> **QUERY**

```
query EmployeeList($where: EmployeeDtoFilterInput) {
  employeeList(where: $where) {
    ...EmployeeDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "where": EmployeeDtoFilterInput,
}
```

# Employee

> **QUERY**

```
query Employee($id: Uuid!) {
  employee(id: $id) {
    ...EmployeeDto
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
