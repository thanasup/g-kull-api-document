[Back to readme](../README.md)

> **SCHEMA**

- [AgentDtoSortInput](../schema/inputs/invoice.md#agentdtosortinput)
- [AgentDtoFilterInput](../schema/inputs/invoice.md#agentdtofilterinput)

# AgentPagedList

> **QUERY**

```
query AgentPagedList($skip: Int, $take: Int, $order: [AgentDtoSortInput!], $where: AgentDtoFilterInput) {
  agentPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      taxId
      code
      name
      description
      id
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    AgentDtoSortInput!
  ] ,
  "where": AgentDtoFilterInput,
}
```

# AgentList

> **QUERY**

```
query AgentList($where: AgentDtoFilterInput, $order: [AgentDtoSortInput!]) {
  agentList(where: $where, order: $order) {
    taxId
    code
    name
    description
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    AgentDtoSortInput!
  ] ,
  "where": AgentDtoFilterInput,
}
```

# Agent

> **QUERY**

```
query Agent($id: Uuid!) {
  agent(id: $id) {
    taxId
    code
    name
    description
    id
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
