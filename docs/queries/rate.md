[Back to readme](../README.md)

> **SCHEMA**

- [MasterRateDtoSortInput](../schema/inputs/master.md#masterratedtosortinput)
- [MasterRateDtoFilterInput](../schema/inputs/master.md#masterratedtofilterinput)
- [MasterRateDto](../schema/objects/master.md#masterratedto)

# RatePagedList

> **QUERY**

```
query RatePagedList($skip: Int, $take: Int, $order: [MasterRateDtoSortInput!], $where: MasterRateDtoFilterInput) {
  ratePagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...MasterRateDto
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterRateDtoSortInput!
  ] ,
  "where": MasterRateDtoFilterInput,
}
```

# RateList

> **QUERY**

```
query RateList($order: [MasterRateDtoSortInput!], $where: MasterRateDtoFilterInput) {
  rateList(order: $order, where: $where) {
    ...MasterRateDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterRateDtoSortInput!
  ] ,
  "where": MasterRateDtoFilterInput,
}
```

# Rate

> **QUERY**

```
query Rate($id: Uuid!) {
  rate(id: $id) {
    ...MasterRateDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```

# RateMin

> **QUERY**

```
query rateMin($input: [RateRegionalFilterDtoInput], $vehicleId: Uuid!) {
  rateMin(input: $input, vehicleId: $vehicleId) {
    ...MasterRateDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "source": {
      "countryId": "",
      "provinceId": "",
      "cityId": "",
      "areaId": "",
      "postalCodeId": ""
    },
    "destination": {
      "countryId": "",
      "provinceId": "",
      "cityId": "",
      "areaId": "",
      "postalCodeId": ""
    },
    "seqNo": 0
  },
  "vehicleId": ""
}
```

# RateMax

> **QUERY**

```
query rateMax($input: [RateRegionalFilterDtoInput], $vehicleId: Uuid!) {
  rateMax(input: $input, vehicleId: $vehicleId) {
    ...MasterRateDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "input": {
    "source": {
      "countryId": "",
      "provinceId": "",
      "cityId": "",
      "areaId": "",
      "postalCodeId": ""
    },
    "destination": {
      "countryId": "",
      "provinceId": "",
      "cityId": "",
      "areaId": "",
      "postalCodeId": ""
    },
    "seqNo": 0
  },
  "vehicleId": ""
}
```
