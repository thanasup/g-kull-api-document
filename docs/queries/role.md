[Back to readme](../README.md)

> **SCHEMA**

- [RoleDtoSortInput](../schema/inputs/role.md#roledtosortinput)
- [RoleDtoFilterInput](../schema/inputs/role.md#roledtofilterinput)
- [RoleDto](../schema/objects/role.md#roledto)

# RolePagedList

> **QUERY**

```
query rolePagedList($skip: Int, $take: Int, $order: [RoleDtoSortInput!], $where: RoleDtoFilterInput) {
  rolePagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...RoleDto
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    RoleDtoSortInput!
  ] ,
  "where": RoleDtoFilterInput,
}
```

# RoleList

> **QUERY**

```
query roleList($order: [RoleDtoSortInput!], $where: RoleDtoFilterInput) {
  roleList(order: $order, where: $where) {
    ...RoleDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    RoleDtoSortInput!
  ] ,
  "where": RoleDtoFilterInput,
}
```

# Role

> **QUERY**

```
query role {
  role {
    ...RoleDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{}
```
