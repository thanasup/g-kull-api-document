[Back to readme](../README.md)

> **SCHEMA**

- [MasterReasonStatusDtoSortInput](../schema/inputs/master.md#masterreasonstatusdtosortinput)
- [MasterReasonStatusDtoFilterInput](../schema/inputs/master.md#masterreasonstatusdtofilterinput)
- [MasterReasonStatusDto](../schema/objects/master.md#masterreasonstatusdto)
- [masterStatus](../schema/inputs/master.md#masterstatusadddtoinput)

# ReasonStatusPagedList

> **QUERY**

```
query ReasonStatusPagedList($skip: Int, $take: Int, $order: [MasterReasonStatusDtoSortInput!], $where: MasterReasonStatusDtoFilterInput) {
  reasonStatusPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...MasterReasonStatusDto
      masterStatus {
        code
        name
      }
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterReasonStatusDtoSortInput!
  ] ,
  "where": MasterReasonStatusDtoFilterInput,
}
```

# ReasonStatusList

> **QUERY**

```
query ReasonStatusList($order: [MasterReasonStatusDtoSortInput!], $where: MasterReasonStatusDtoFilterInput) {
  reasonStatusList(order: $order, where: $where) {
    ...MasterReasonStatusDto
    masterStatus {
      code
      name
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterReasonStatusDtoSortInput!
  ] ,
  "where": MasterReasonStatusDtoFilterInput,
}
```

# ReasonStatus

> **QUERY**

```
query ReasonStatus($id: Uuid!) {
  reasonStatus(id: $id) {
    ...MasterReasonStatusDto
    masterStatus {
      code
      name
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
