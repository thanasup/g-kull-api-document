[Back to readme](../README.md)

> **SCHEMA**

- [DriverAddressDtoSortInput](../schema/inputs/drider.md#driveraddressdtosortinput)
- [DriverAddressDtoFilterInput](../schema/inputs/drider.md#driveraddressdtofilterinput)

# DriverAddressPagedList

> **QUERY**

```
query driverAddressPagedList($skip: Int, $take: Int, $order: [DriverAddressDtoSortInput!], $where: DriverAddressDtoFilterInput) {
  driverAddressPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      driverId
      isDefault
      addressType
      addressTypeList
      address
      countryId
      provinceId
      cityId
      areaId
      postalCodeId
      tel
      phone
      email
      gpsLat
      gpsLon
      country {
        code
        name
        nameTh
      }
      province {
        code
        name
        nameTh
      }
      city {
        code
        name
        nameTh
      }
      area {
        code
        name
        nameTh
      }
      postalCode {
        code
      }
      id
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    DriverAddressDtoSortInput!
  ] ,
  "where": DriverAddressDtoFilterInput,
}
```

# DriverAddressList

> **QUERY**

```
query driverAddressList($where: DriverAddressDtoFilterInput, $order: [DriverAddressDtoSortInput!]) {
  driverAddressList(order: $order, where: $where) {
    driverId
    isDefault
    addressType
    addressTypeList
    address
    countryId
    provinceId
    cityId
    areaId
    postalCodeId
    tel
    phone
    email
    gpsLat
    gpsLon
    country {
      code
      name
      nameTh
    }
    province {
      code
      name
      nameTh
    }
    city {
      code
      name
      nameTh
    }
    area {
      code
      name
      nameTh
    }
    postalCode {
      code
    }
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    DriverAddressDtoSortInput!
  ] ,
  "where": DriverAddressDtoFilterInput,
}
```

# DriverAddress

> **QUERY**

```
query driverAddress($addressId: Uuid!) {
  driverAddress(addressId: $addressId) {
    driverId
    isDefault
    addressType
    addressTypeList
    address
    countryId
    provinceId
    cityId
    areaId
    postalCodeId
    tel
    phone
    email
    gpsLat
    gpsLon
    countryId
    country {
      code
      name
      nameTh
    }
    provinceId
    province {
      code
      name
      nameTh
    }
    cityId
    city {
      code
      name
      nameTh
    }
    areaId
    area {
      code
      name
      nameTh
    }
    postalCodeId
    postalCode {
      code
    }
    id
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": ""
}
```
