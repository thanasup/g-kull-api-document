[Back to readme](../README.md)

> **SCHEMA**

- [OrderPriceDtoSortInput](../schema/inputs/order.md#orderpricedtosortinput)
- [OrderPriceDtoFilterInput](../schema/inputs/order.md#orderpricedtofilterinput)
- [OrderPriceDto](../schema/objects/order.md#orderpricedto)

# OrderPrice

> **QUERY**

```
query orderPrice($orderId: [Uuid!], $where: OrderPriceDtoFilterInput, $order: [OrderPriceDtoSortInput!]) {
  orderPrice(orderId: $orderId, where: $where, order: $order) {
    ...OrderPriceDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "orderId": [
    ""
  ],
  "order": [
    OrderPriceDtoSortInput!
  ] ,
  "where": OrderPriceDtoFilterInput,
}
```

# OrderPricePagedList

> **QUERY**

```
query orderPricePagedList($skip: Int, $take: Int, $order: [OrderPriceDtoSortInput!], $where: OrderPriceDtoFilterInput) {
  orderPricePagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...OrderPriceDto
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    OrderPriceDtoSortInput!
  ] ,
  "where": OrderPriceDtoFilterInput,
}
```
