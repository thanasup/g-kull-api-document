[Back to readme](../README.md)

> **SCHEMA**

- [InvoiceHdDtoSortInput](../schema/inputs/invoice.md#invoicehddtosortinput)
- [InvoiceHdDtoFilterInput](../schema/inputs/invoice.md#invoicehddtofilterinput)
- [InvoiceHdDto](../schema/objects/invoice.md#invoicehddto)

# InvoicePagedList

> **QUERY**

```
query invoicePagedList($skip: Int, $take: Int, $order: [InvoiceHdDtoSortInput!], $where: InvoiceHdDtoFilterInput) {
  invoicePagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...InvoiceHdDto
      createdDate
      createdBy
      updatedDate
      updatedBy
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    InvoiceHdDtoSortInput!
  ] ,
  "where": InvoiceHdDtoFilterInput,
}
```

# InvoiceList

> **QUERY**

```
query invoiceList($order: [InvoiceHdDtoSortInput!], $where: InvoiceHdDtoFilterInput) {
  invoiceList(order: $order, where: $where) {
    ...InvoiceHdDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    InvoiceHdDtoSortInput!
  ] ,
  "where": InvoiceHdDtoFilterInput,
}
```

# Invoice

> **QUERY**

```
query invoice($id: Uuid!) {
  invoice(id: $id) {
    ...InvoiceHdDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
