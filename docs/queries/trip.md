[Back to readme](../README.md)

> **SCHEMA**

- [TripHdDtoSortInput](../schema/inputs/trip.md#triphddtosortinput)
- [TripHdDtoFilterInput](../schema/inputs/trip.md#triphddtofilterinput)
- [TripList](../schema/objects/trip.md#triphddto)
- [TripDto](../schema/objects/trip.md#triphddto)

# TripPagedList

> **QUERY**

```
query tripPagedList($skip: Int, $take: Int, $order: [TripHdDtoSortInput!], $where: TripHdDtoFilterInput) {
  tripPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...TripList
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
      id
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    TripHdDtoSortInput!
  ] ,
  "where": TripHdDtoFilterInput,
}
```

# TripList

> **QUERY**

```
query tripList($order: [TripHdDtoSortInput!], $where: TripHdDtoFilterInput) {
  tripList(order: $order, where: $where) {
    ...TripList
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    TripHdDtoSortInput!
  ] ,
  "where": TripHdDtoFilterInput,
}
```

# Trip

> **QUERY**

```
query trip($id: Uuid!) {
  trip(id: $id) {
    ...TripDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
