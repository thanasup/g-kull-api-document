[Back to readme](../README.md)

> **SCHEMA**

- [EmployeeAddressDtoSortInput](../schema/inputs/employee.md#employeeaddressdtosortinput)
- [EmployeeAddressDtoFilterInput](../schema/inputs/employee.md#employeeaddressdtofilterinput)

# EmployeeAddressPagedList

> **QUERY**

```
query employeeAddressPagedList($skip: Int, $take: Int, $order: [EmployeeAddressDtoSortInput!], $where: EmployeeAddressDtoFilterInput) {
  employeeAddressPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      employeeId
      isDefault
      addressType
      addressTypeList
      address
      countryId
      provinceId
      cityId
      areaId
      postalCodeId
      tel
      phone
      email
      gpsLat
      gpsLon
      country {
        code
        name
        nameTh
      }
      province {
        code
        name
        nameTh
      }
      city {
        code
        name
        nameTh
      }
      area {
        code
        name
        nameTh
      }
      postalCode {
        code
      }
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
      id
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    EmployeeAddressDtoSortInput!
  ] ,
  "where": EmployeeAddressDtoFilterInput,
}
```

# EmployeeAddressList

> **QUERY**

```
query employeeAddressList($where: EmployeeAddressDtoFilterInput, $order: [EmployeeAddressDtoSortInput!]) {
  employeeAddressList(where: $where, order: $order) {
    employeeId
    isDefault
    addressType
    addressTypeList
    address
    countryId
    provinceId
    cityId
    areaId
    postalCodeId
    tel
    phone
    email
    gpsLat
    gpsLon
    country {
      code
      name
      nameTh
    }
    province {
      code
      name
      nameTh
    }
    city {
      code
      name
      nameTh
    }
    area {
      code
      name
      nameTh
    }
    postalCode {
      code
    }
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    EmployeeAddressDtoSortInput!
  ] ,
  "where": EmployeeAddressDtoFilterInput,
}
```

# EmployeeAddress

> **QUERY**

```
query employeeAddress($addressId: Uuid!) {
  employeeAddress(addressId: $addressId) {
    employeeId
    isDefault
    addressType
    addressTypeList
    address
    countryId
    provinceId
    cityId
    areaId
    postalCodeId
    tel
    phone
    email
    gpsLat
    gpsLon
    country {
      code
      name
      nameTh
    }
    province {
      code
      name
      nameTh
    }
    city {
      code
      name
      nameTh
    }
    area {
      code
      name
      nameTh
    }
    postalCode {
      code
    }
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": ""
}
```
