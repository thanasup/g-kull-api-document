[Back to readme](../README.md)

> **SCHEMA**

- [CustomerDtoSortInput](../schema/inputs/customer.md#customerdtosortinput)
- [CustomerDtoFilterInput](../schema/inputs/customer.md#customerdtofilterinput)
- [CustomerDto](../schema/objects/customer.md#customerdto)

# CustomerPagedList

> **QUERY**

```
query CustomerPagedList($skip: Int, $take: Int, $order: [CustomerDtoSortInput!], $where: CustomerDtoFilterInput) {
  customerPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...CustomerDto
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    CustomerDtoSortInput!
  ] ,
  "where": CustomerDtoFilterInput,
}
```

# CustomerList

> **QUERY**

```
query CustomerList($where: CustomerDtoFilterInput) {
  customerList(where: $where) {
    ...CustomerDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "where": CustomerDtoFilterInput,
}
```

# Agent

> **QUERY**

```
query Customer($id: Uuid!) {
  customer(id: $id) {
    ...CustomerDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
