[Back to readme](../README.md)

> **SCHEMA**

- [AgentVehicleDtoSortInput](../schema/inputs/agent.md#agentvehicledtosortinput)
- [AgentVehicleDtoFilterInput](../schema/inputs/agent.md#agentvehicledtofilterinput)

# AgentVehiclePagedList

> **QUERY**

```
query agentVehiclePagedList($skip: Int, $take: Int, $order: [AgentVehicleDtoSortInput!], $where: AgentVehicleDtoFilterInput) {
  agentVehiclePagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      caution
      serviceCondition
      agentId
      vehicleId
      license
      driverId
      hasDim
      length
      width
      height
      weight
      maxPayLoad
      maxLiftingWeight
      cbm
      onlyOneArticle
      vehicle {
        freeTime
        hasDim
        length
        width
        height
        weight
        maxPayLoad
        maxLiftingWeight
        cbm
        code
        name
        description
      }
      driver {
        isApprove
        idCard
        drivingLicenseId
        dateOfBirth
        sex
        age
        code
        fullName
        phone
        email
      }
      id
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    AgentVehicleDtoSortInput!
  ] ,
  "where": AgentVehicleDtoFilterInput,
}
```

# AgentVehicleList

> **QUERY**

```
query agentVehicleList($where: AgentVehicleDtoFilterInput, $order: [AgentVehicleDtoSortInput!]) {
  agentVehicleList(where: $where, order: $order) {
    caution
    serviceCondition
    agentId
    vehicleId
    license
    driverId
    hasDim
    length
    width
    height
    weight
    maxPayLoad
    maxLiftingWeight
    cbm
    onlyOneArticle
    vehicle {
      freeTime
      hasDim
      length
      width
      height
      weight
      maxPayLoad
      maxLiftingWeight
      cbm
      code
      name
      description
    }
    driver {
      isApprove
      idCard
      drivingLicenseId
      dateOfBirth
      sex
      age
      code
      fullName
      phone
      email
    }
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    AgentVehicleDtoSortInput!
  ] ,
  "where": AgentVehicleDtoFilterInput,
}
```

# AgentVehicle

> **QUERY**

```
query AgentVehicle($vehicleId: Uuid!) {
  agentVehicle(vehicleId: $vehicleId) {
    caution
    serviceCondition
    agentId
    vehicleId
    license
    driverId
    hasDim
    length
    width
    height
    weight
    maxPayLoad
    maxLiftingWeight
    cbm
    onlyOneArticle
    vehicle {
      freeTime
      hasDim
      length
      width
      height
      weight
      maxPayLoad
      maxLiftingWeight
      cbm
      code
      name
      description
    }
    driver {
      isApprove
      idCard
      drivingLicenseId
      dateOfBirth
      sex
      age
      code
      fullName
      phone
      email
    }
    id
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "vehicleId": ""
}
```
