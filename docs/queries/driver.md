[Back to readme](../README.md)

> **SCHEMA**

- [DriverDtoSortInput](../schema/inputs/driver.md#driverdtosortinput)
- [DriverDtoFilterInput](../schema/inputs/driver.md#driverdtofilterinput)
- [DriverDto](../schema/objects/driver.md#driverdto)

# DriverPagedList

> **QUERY**

```
query DriverPagedList($skip: Int, $take: Int, $order: [DriverDtoSortInput!], $where: DriverDtoFilterInput) {
  driverPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...DriverDto
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    DriverDtoSortInput!
  ] ,
  "where": DriverDtoFilterInput,
}
```

# DriverList

> **QUERY**

```
query DriverList($where: DriverDtoFilterInput, $order: [DriverDtoSortInput!]) {
  driverList(order: $order, where: $where) {
    ...DriverDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    DriverDtoSortInput!
  ] ,
  "where": DriverDtoFilterInput,
}
```

# Driver

> **QUERY**

```
query Driver($id: Uuid!) {
  driver(id: $id) {
    ...DriverDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
