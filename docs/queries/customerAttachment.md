[Back to readme](../README.md)

> **SCHEMA**

- [CustomerAttachmentDtoSortInput](../schema/inputs/customer.md#customerattachmentdtosortinput)
- [CustomerAttachmentDtoFilterInput](../schema/inputs/customer.md#customerattachmentdtofilterinput)
- [CustomerAttachmentDto](../schema/objects/customer.md#customerattachmentdto)

# CustomerAttachmentPagedList

> **QUERY**

```
query customerAttachmentPagedList($skip: Int, $take: Int, $order: [CustomerAttachmentDtoSortInput!], $where: CustomerAttachmentDtoFilterInput) {
  customerAttachmentPagedList(
    skip: $skip
    take: $take
    order: $order
    where: $where
  ) {
    items {
      ...CustomerAttachmentDto
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    CustomerAttachmentDtoSortInput!
  ] ,
  "where": CustomerAttachmentDtoFilterInput,
}
```

# CustomerAttachmentList

> **QUERY**

```
query customerAttachmentList($where: CustomerAttachmentDtoFilterInput) {
  customerAttachmentList(where: $where) {
    ...CustomerAttachmentDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "where": CustomerAttachmentDtoFilterInput,
}
```
