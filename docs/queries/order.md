[Back to readme](../README.md)

> **SCHEMA**

- [OrderDtoSortInput](../schema/inputs/order.md#orderdtosortinput)
- [OrderDtoFilterInput](../schema/inputs/order.md#orderdtofilterinput)
- [OrderList](../schema/objects/order.md#orderdto)
- [OrderDto](../schema/objects/order.md#orderdto)
- [OrderProveOfDeliveryDto](../schema/objects/order.md#orderproveofdeliverydto)

# OrderPagedList

> **QUERY**

```
query orderPagedList($skip: Int, $take: Int, $order: [OrderDtoSortInput!], $where: OrderDtoFilterInput) {
  orderPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...OrderList
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    OrderDtoSortInput!
  ] ,
  "where": OrderDtoFilterInput,
}
```

# OrderList

> **QUERY**

```
query orderList($order: [OrderDtoSortInput!], $where: OrderDtoFilterInput) {
  orderList(order: $order, where: $where) {
    ...OrderList
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    OrderDtoSortInput!
  ] ,
  "where": OrderDtoFilterInput,
}
```

# Order

> **QUERY**

```
query order($id: Uuid!) {
  order(id: $id) {
    ...OrderDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```

# OrderAttachment

> **QUERY**

```
query orderAttachment($orderId: Uuid!) {
  orderAttachment(orderId: $orderId) {
    ...OrderProveOfDeliveryDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "orderId": ""
}
```