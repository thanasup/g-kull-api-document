[Back to readme](../README.md)

> **SCHEMA**

- [CustomerProjectDtoSortInput](../schema/inputs/customer.md#customerprojectdtosortinput)
- [CustomerProjectDtoFilterInput](../schema/inputs/customer.md#customerprojectdtofilterinput)
- [CustomerProjectDto](../schema/objects/customer.md#customerprojectdto)

# CustomerProjectPagedList

> **QUERY**

```
query customerProjectPagedList($skip: Int, $take: Int, $order: [CustomerProjectDtoSortInput!], $where: CustomerProjectDtoFilterInput) {
  customerProjectPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      ...CustomerProjectDto
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    CustomerProjectDtoSortInput!
  ] ,
  "where": CustomerProjectDtoFilterInput,
}
```

# CustomerProjectList

> **QUERY**

```
query customerProjectList($where: CustomerProjectDtoFilterInput) {
  customerProjectList(where: $where) {
    ...CustomerProjectDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "where": CustomerProjectDtoFilterInput,
}
```

# CustomerProject

> **QUERY**

```
query customerProject($projectId: Uuid!) {
  customerProject(projectId: $projectId) {
    ...CustomerProjectDto
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "projectId": ""
}
```
