[Back to readme](../README.md)

> **SCHEMA**

- [AgentAddressDtoSortInput](../schema/inputs/agent.md#agentaddressdtosortinput)
- [AgentAddressDtoFilterInput](../schema/inputs/agent.md#agentaddressdtofilterinput)

# AgentAddressPagedList

> **QUERY**

```
query AgentAddressPagedList($skip: Int, $take: Int, $order: [AgentAddressDtoSortInput!], $where: AgentAddressDtoFilterInput) {
  agentAddressPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      isDefault
      addressType
      addressTypeList
      address
      tel
      phone
      email
      gpsLat
      gpsLon
      country {
        code
        name
        nameTh
      }
      province {
        code
        name
        nameTh
      }
      city {
        code
        name
        nameTh
      }
      area {
        code
        name
        nameTh
      }
      postalCode {
        code
      }
      id
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    AgentAddressDtoSortInput!
  ] ,
  "where": AgentAddressDtoFilterInput,
}
```

# AgentAddressList

> **QUERY**

```
query AgentAddressList($where: AgentAddressDtoFilterInput, $order: [AgentAddressDtoSortInput!]) {
  agentAddressList(where: $where, order: $order) {
    isDefault
    addressType
    addressTypeList
    address
    tel
    phone
    email
    gpsLat
    gpsLon
    countryId
    country {
      code
      name
      nameTh
    }
    provinceId
    province {
      code
      name
      nameTh
    }
    cityId
    city {
      code
      name
      nameTh
    }
    areaId
    area {
      code
      name
      nameTh
    }
    postalCodeId
    postalCode {
      code
    }
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    AgentAddressDtoSortInput!
  ] ,
  "where": AgentAddressDtoFilterInput,
}
```

# AgentAddress

> **QUERY**

```
query AgentAddress($addressId: Uuid!) {
  agentAddress(addressId: $addressId) {
    isDefault
    addressType
    addressTypeList
    address
    tel
    phone
    email
    gpsLat
    gpsLon
    countryId
    country {
      code
      name
      nameTh
    }
    provinceId
    province {
      code
      name
      nameTh
    }
    cityId
    city {
      code
      name
      nameTh
    }
    areaId
    area {
      code
      name
      nameTh
    }
    postalCodeId
    postalCode {
      code
    }
    id
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "addressId": ""
}
```
