[Back to readme](../README.md)

> **SCHEMA**

- [DriverVehicleDtoSortInput](../schema/inputs/driver.md#drivervehicledtosortinput)
- [DriverVehicleDtoFilterInput](../schema/inputs/driver.md#drivervehicledtofilterinput)

# DriverVehiclePagedList

> **QUERY**

```
query driverVehiclePagedList($skip: Int, $take: Int, $order: [DriverVehicleDtoSortInput!], $where: DriverVehicleDtoFilterInput) {
  driverVehiclePagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      caution
      serviceCondition
      driverId
      vehicleId
      license
      hasDim
      length
      width
      height
      weight
      maxPayLoad
      maxLiftingWeight
      cbm
      onlyOneArticle
      id
      vehicle {
        freeTime
        hasDim
        length
        width
        height
        weight
        maxPayLoad
        maxLiftingWeight
        cbm
        code
        name
        description
      }
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    DriverVehicleDtoSortInput!
  ] ,
  "where": DriverVehicleDtoFilterInput,
}
```

# DriverVehicleList

> **QUERY**

```
query driverVehicleList($where: DriverVehicleDtoFilterInput, $order: [DriverVehicleDtoSortInput!]) {
  driverVehicleList(order: $order, where: $where) {
    caution
    serviceCondition
    driverId
    vehicleId
    license
    hasDim
    length
    width
    height
    weight
    maxPayLoad
    maxLiftingWeight
    cbm
    onlyOneArticle
    id
    vehicle {
      freeTime
      hasDim
      length
      width
      height
      weight
      maxPayLoad
      maxLiftingWeight
      cbm
      code
      name
      description
    }
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    DriverVehicleDtoSortInput!
  ] ,
  "where": DriverVehicleDtoFilterInput,
}
```

# DriverVehicle

> **QUERY**

```
query driverVehicle($vehicleId: Uuid!) {
  driverVehicle(vehicleId: $vehicleId) {
    caution
    serviceCondition
    driverId
    vehicleId
    license
    hasDim
    length
    width
    height
    weight
    maxPayLoad
    maxLiftingWeight
    cbm
    onlyOneArticle
    id
    vehicle {
      freeTime
      hasDim
      length
      width
      height
      weight
      maxPayLoad
      maxLiftingWeight
      cbm
      code
      name
      description
    }
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "vehicleId": ""
}
```
