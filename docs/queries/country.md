[Back to readme](../README.md)

> **SCHEMA**

- [MasterCountryDtoSortInput](../schema/inputs/master.md#mastercountrydtosortinput)
- [MasterCountryDtoFilterInput](../schema/inputs/master.md#mastercountrydtofilterinput)

# CountryPagedList

> **QUERY**

```
query CountryPagedList($skip: Int, $take: Int, $order: [MasterCountryDtoSortInput!], $where: MasterCountryDtoFilterInput) {
  countryPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      code
      name
      nameTh
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
      id
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterCountryDtoSortInput!
  ] ,
  "where": MasterCountryDtoFilterInput,
}
```

# CountryList

> **QUERY**

```
query CountryList($order: [MasterCountryDtoSortInput!], $where: MasterCountryDtoFilterInput) {
  countryList(order: $order, where: $where) {
    code
    name
    nameTh
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterCountryDtoSortInput!
  ] ,
  "where": MasterCountryDtoFilterInput,
}
```

# Country

> **QUERY**

```
query Country($id: Uuid!) {
  country(id: $id) {
    code
    name
    nameTh
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
