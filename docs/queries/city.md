[Back to readme](../README.md)

> **SCHEMA**

- [MasterCityDtoSortInput](../schema/inputs/master.md#mastercitydtosortinput)
- [MasterCityDtoFilterInput](../schema/inputs/master.md#mastercitydtofilterinput)
- [country](../schema/inputs/master.md#mastercountryadddtoinput)
- [province](../schema/inputs/master.md#masterprovinceadddtoinput)

# CityPagedList

> **QUERY**

```
query CityPagedList($skip: Int, $take: Int, $order: [MasterCityDtoSortInput!], $where: MasterCityDtoFilterInput) {
  cityPagedList(skip: $skip, take: $take, order: $order, where: $where) {
    items {
      code
      name
      nameTh
      country {
        name
        nameTh
      }
      province {
        name
        nameTh
      }
      isActive
      isDelete
      createdDate
      createdBy
      updatedDate
      updatedBy
      id
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
    }
    totalCount
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "skip": 0,
  "take": 0,
  "order": [
    MasterCityDtoSortInput!
  ] ,
  "where": MasterCityDtoFilterInput,
}
```

# CityList

> **QUERY**

```
query CityList($where: MasterCityDtoFilterInput, $order: [MasterCityDtoSortInput!]) {
  cityList(where: $where, order: $order) {
    code
    name
    nameTh
    country {
      name
      nameTh
    }
    province {
      name
      nameTh
    }
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterCityDtoSortInput!
  ] ,
  "where": MasterCityDtoFilterInput,
}
```

# City

> **QUERY**

```
query City($id: Uuid!) {
  city(id: $id) {
    code
    name
    nameTh
    countryId
    country {
      name
      nameTh
    }
    provinceId
    province {
      name
      nameTh
    }
    isActive
    isDelete
    createdDate
    createdBy
    updatedDate
    updatedBy
    id
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "id": ""
}
```
