[Back to readme](../README.md)

> **SCHEMA**

- [MasterContainerLocationDtoFilterInput](../schema/inputs/master.md#mastercontainerlocationdtosortinput)
- [MasterContainerLocationDtoSortInput](../schema/inputs/master.md#mastercontainerlocationdtofilterinput)
- [MasterContainerLocationDto](../schema/objects/master.md#mastercontainerlocationdto)

# ContainerLocationList

> **QUERY**

```
query containerLocationList($where: MasterContainerLocationDtoFilterInput, $order: [MasterContainerLocationDtoSortInput!]) {
  containerLocationList(where: $where, order: $order) {
    ...MasterContainerLocationDto
  }
}
```

> **GRAPHQL VARIABLES**

```json
{
  "order": [
    MasterContainerLocationDtoSortInput!
  ] ,
  "where": MasterContainerLocationDtoFilterInput,
}
```
